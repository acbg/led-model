##############################
# Import packages and modules
##############################
import time
stime = time.time()
print('Elapsed time {0:.3f}: Initialise'.format(time.time() - stime))
import warnings
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.special import lambertw
import importlib

import functions as funs
import initVals as ini
from stack import ledStack

import sys
sys.path.append('../') # access to all other packages in the upper directory
from physics.constants import ureg, Q_
import physics.constants as pc
from curvesAnalysis.main import deviceList, devsDF, devsDFsimple, devsDFfilt
#  curvesAnalysis = importlib.import_module('../curves-analysis.main', None)
#  from curvesAnalysis import deviceList, devsDF, devsDFsimple, devsDFfilt

print('Elapsed time {0:.3f}: Loaded modules'.format(time.time() - stime))

###########################
# Modules/packages options
###########################
warnings.filterwarnings("ignore")

np.set_printoptions(threshold=sys.maxsize)

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 0)

#  plt.style.use('bmh')
#  plt.style.use('tableau-colorblind')
#  plt.style.use('fivethirtyeight')
#  plt.style.use('seaborn-darkgrid')
#  plt.style.use('seaborn-colorblind')
#  plt.style.use('dark_background')
#  plt.style.use('ggplot')
plt.style.use('fast')
plt.rcParams.update({
    #  'font.family'       : 'arial',
    'text.usetex'       : True,
    'axes.edgecolor'    : 'dimgrey',
    'axes.grid'         : False,
    'font.size'         : 6.5,
    'lines.linewidth'   : 1.0,
    'lines.markersize'  : 1.0,
    'legend.shadow'     : False,
    'legend.fancybox'   : False,
    })


#####################
# Plotting Functions
#####################

def PlotECcomponents(
        expDeviceList,
        device,
        savePlot=False, fileName=False, fileFrmt='jpg', showPlot=True
    ):
    '''
    Effect of each circuit component on the final IV curve
    '''
    for dev in expDeviceList:
        #  if dev.sampleName == 'TPX0342' and dev.samplePID == 'S130' and dev.name =='500A4':
        if dev.sampleName == 'TPX0342' and dev.samplePID == 'S130' and dev.name =='250F7':
            expDev = dev

    fig, ax = plt.subplots(2, 2)
    fig.suptitle(f'Simulated: $D={device.diameter:.0f~L}, d_T={device.dT:.0f~L},  d_M={device.dM:.0f~L}$ \n\
            Exp.: {expDev.sampleName} - {expDev.samplePID} - {expDev.name}:$D={expDev.diameter:.0f~L},d_T={expDev.dT:.0f~L}, d_M={expDev.dM:.0f~L}$')

    # Current components
    # ------------------
    axi = ax[0,0]
    axi.semilogy( device.u, abs(device.i), 'b-',
            label = f'$I_L$'
        )

    # Exp. LED response
    axi.semilogy( expDev.u, abs(expDev.iL), 'bo',
            label = f'$I_L (exp)$'
        )

    # AR Radiative
    axi.semilogy( device.u, abs(device.iARrad), 'r--',
            label = f'$I_{{AR}}^{{Rad}}$'
        )
    #  colormap = plt.cm.Greens
    #  colors = [ colormap(x) for x in np.linspace(0, 1, device.ARNdio) ]
    #  for i in range(device.ARNdio):
    #      axi.semilogy( device.u, abs(device.iARradDio[i]), '--', c=colors[i],
    #              label = f'$I_{{AR}}^{{Rad}} (D{i:.0f}$')

    # Exp. PD response
    axi.semilogy( expDev.u, abs(expDev.iD), 'ro',
            label = f'$I_D (exp)$'
        )

    # AR Non-radiative
    axi.semilogy( device.u, abs(device.iARsrh), 'g:',
            label = f'$I_{{AR}}^{{SRH}}$'
        )

    #  colormap = plt.cm.Reds
    #  colors = [ colormap(x) for x in np.linspace(0, 1, device.ARNdio) ]
    #  for i in range(device.ARNdio):
    #      axi.semilogy( device.u, abs(device.iARsrhDio[i]), '--', c=colors[i],
    #              label = f'$I_{{AR}}^{{Rad}} (D{i:.0f}$')

    # AR Auger
    axi.semilogy( device.u, abs(device.iARaug), 'k-.',
            label = f'$I_{{AR}}^{{Aug}}$'
        )

    #  AR diode current
    axi.semilogy( device.u, abs(device.iAR), '-',
            label='$I_{AR}$'
        )

    # SF diode current
    axi.semilogy( device.u, abs(device.iSF), '-.',
            label = f'$I_{{SF}}$'
        )

    #  axi.semilogy(
    #          device.u.to_base_units().magnitude,
    #          abs(device.iSh.to_base_units().magnitude),
    #          '-',
    #          label = f'$I_{{SH}} (R_{{SH}} = {device.rSh: .0f~L}$)'
    #      )

    # Text annotation with initial values of different parameters
    textstr = '\n'.join((
        f'$A       = {device.A: 0.1e~L}$',
        f'$B       = {device.B: 0.1e~L}$',
        f'$C       = {device.C: 0.1e~L}$',
        f'$I_{{SF}} = {device.i0SF: .1e~L}$',
        f'$R_{{SF}} = {device.rSF.to(ureg.ohm): 0.0f~L}$',
        f'$d(AR) = {device.ARthickness: .1f~L}$',
        f'$N_{{ARdiodes}} = {device.ARNdio: .0f}$',
        ))
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    axi.text(
                0.02, 0.98, textstr,
                transform=axi.transAxes,
                fontsize=3,
                verticalalignment='top', horizontalalignment='left',
                bbox=props
            )

    axi.set_title('Current components')
    axi.set_xlabel('$U$')
    axi.set_ylabel('$I$')
    #  axi.set_xlim([-1.5,4.0])
    axi.set_xlim([-0.0,4.0])
    axi.set_ylim([1e-9,10])
    axi.legend(loc='best', ncol=1)

    #  # Voltages
    #  # --------
    axi = ax[0,1]
    axi.plot(device.uAR, device.uSF, '-o', label='$U_{SF}$')
    axi.plot(device.uAR, device.uAR, label='$U_{AR}$')
    axi.plot(device.uAR, device.uSP, label = '$U_{SP}$')
    axi.plot(device.uAR, device.u, label = '$U_L$')

    axi.set_title('Voltages')
    axi.set_xlabel('$U$')
    axi.set_ylabel('$U$')
    axi.legend(loc='best')
    #  axi.set_xlim([-1.5,4.0])
    axi.set_xlim([-0.0,4.0])
    axi.set_ylim([0,10])

    #  # Efficiencies
    #  # ------------
    axi = ax[1,0]
    axi.plot(expDev.u, expDev.cqe, 'o', label=f'$CQE (exp)$')
    axi.plot(device.u, device.eqe, '-', label=f'$EQE (\chi={device.chi:.1f})$')
    axi.plot(device.u, device.injEff, ':', label='$\eta_{inj}$')
    axi.plot(device.u, device.iqe, '--', label = '$IQE$')

    axi.set_title('Efficiencies')
    axi.set_xlabel('$U$')
    axi.set_ylabel('$QE$')
    axi.legend(loc='best')
    axi.set_xlim([-0.0,4])
    axi.set_ylim([0,1])

    #  # carrier density
    #  # ------------
    axi = ax[1,1]
    axi.semilogy(device.u, device.nAR.magnitude, '-', label='$n$')
    axi.semilogy(device.u, device.nAR.magnitude**2, '-', label='$n^2$')
    axi.semilogy(device.u, device.nAR.magnitude**3, '-', label='$n^3$')
    #  for i,val in enumerate(device.nARDio):
    #      axi.semilogy(device.u, device.nARDio[i].magnitude, '-', label=f'$n (D{i:.0f})$')

    axi.set_title('Carrier density')
    axi.set_xlabel('$U$')
    axi.set_ylabel('$n$')
    axi.legend(loc='best')
    #  axi.set_xlim([-1.5,4])
    axi.set_xlim([-0.0,4.0])
    #  axi.set_ylim([1e-6,1e8])

    # Shockley equation
    # -----------------
    #  axi = ax[1,0]
    #  uLED, u, i, iSurf, iL, iSh, iSpread = ec.ledECshockley (us, rSurf, i0S, i0L, rSpread, rSh, i0P, T)
    #  axi.semilogy(u, abs(i), 'c', label=('$I_{Total}$ ($R_{SP}=%.1f$)' %(rSpread)))
    #  axi.semilogy(u, abs(iSurf), 'r', label='$I_{SF}$ ($A_{SF} = %.1e, R_{SF} = %.1f$)' %(i0S, rSurf))
    #  axi.semilogy(u, abs(iL), 'b', label='$I_{LED}$ ($I_{0,LED} = %.1e$)' %(i0L))
    #  axi.semilogy(u, abs(iSh), 'g', label='$I_{SH}$ ($R_{SH} = %.0f$)' %(rSh))
    #  axi.semilogy(u, abs(iSpread), 'y', label='$I_{SP}$ ($R_{SP} = %.1f$)' %(rSpread))
    #  axi.set_xlabel('$U$')
    #  axi.set_ylabel('$I$')
    #  axi.set_xlim([-0.5,3])
    #  axi.set_ylim([1e-9,10])
    #  axi.set_title('Shockley')
    #  axi.legend(loc='best', ncol=1)

    if savePlot:
        fig.savefig(fileName+'.'+fileFrmt, dpi=350)

    if showPlot==True:
        fig.set_dpi(150)

    return fig

def PlotParamsEffect (
        radius, dT, dM, uSurfDiode, stack,
        As, Bs, Cs, Asurfs, dTs, dMs,
        colormap = plt.cm.Greens,
        plotCQE=False,
        title='', savePlot=False, fileName='', fileFrmt='jpg', showPlot=True):
    '''
    Effect of each parameter on the equivalent circuit IV curve
    '''
    # ABC parametrized equation
    # ##########################
    fig, ax = plt.subplots(2, 3)
    fig.suptitle(title)
    # text
    textstr = '\n'.join((
        f'Initial values:',
        f'$A = {ini.A: .1e~L}$',
        f'$B = {ini.B: .1e~L}$',
        f'$C = {ini.C: .1e~L}$',
        f'$A_{{SF}} = {ini.Asurf: .1e~L}$',
        #  f'$R_{{SF}} = {device.rSurf.to(ureg.ohm): .0f~L}$',
        #  f'$R_{{SP}} = {device.rSpread.to(ureg.ohm): .1f~L}$',
        f'$R_{{SH}} = {ini.rSh: .0f~L}$',
        f'$T = {ini.T: .0f~L}$',
        f'$radius = {radius: .0f~L}$',
        f'$d_T = {dT: .0f~L}$',
        f'$d_M = {dM: .0f~L}$',
        ))
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    # axis label names
    xlbl = '$U$'
    ylbl = '$CQE$' if plotCQE else '$I$'
    # axis limits
    xlims = [-0.1,3]
    ylims = [0, 1] if plotCQE else [1e-8,1]

    # Effect of A
    # -----------
    axi = ax[0,0]
    # subplot title
    axi.set_title('Effect of $A$')
    # colors
    colors = [ colormap(x) for x in np.linspace(0, 1, len(As)) ]
    for ii, A in enumerate (As):
        device = funs.LED (**{
                    'chi': 0.6,
                    'radius': radius,
                    'dT': dT,
                    'dM': dM,
                    'uSurfDiode': uSurfDiode,
                    'stack': stack,
                    'ARthickness': ini.ARthickness,
                    'A': A,
                    'B': ini.B,
                    'C': ini.C,
                    'Asurf': ini.Asurf,
                    'Bsurf': ini.Bsurf,
                    'Csurf': ini.Csurf,
                    'T': ini.T,
                    'rSh': ini.rSh,
                })

        lbl = f'$A= {device.A: .1e~L}$'
        plttype = axi.plot if plotCQE else axi.semilogy
        xx = device.u.magnitude
        yy = device.cqe if plotCQE else abs(device.i.magnitude)
        plttype (xx, yy, label = lbl, c = colors[ii])

    # axis labels
    axi.set_xlabel(xlbl), axi.set_ylabel(ylbl)
    # limits
    axi.set_xlim(xlims), axi.set_ylim(ylims)
    # legend
    axi.legend(loc='lower right', ncol=1)
    # text
    axi.text(0.02, 0.98, textstr, transform=axi.transAxes, #fontsize=12,
            verticalalignment='top', bbox=props)


    # Effect of B
    # -----------
    axi = ax[0,1]
    # subplot title
    axi.set_title('Effect of $B$')
    # colors
    colors = [ colormap(x) for x in np.linspace(0, 1, len(Bs)) ]
    for ii, B in enumerate(Bs):
        device = funs.LED (**{
                    'chi': 0.6,
                    'radius': radius,
                    'dT': dT,
                    'dM': dM,
                    'uSurfDiode': uSurfDiode,
                    'stack': stack,
                    'ARthickness': ini.ARthickness,
                    'A': ini.A,
                    'B': B,
                    'C': ini.C,
                    'Asurf': ini.Asurf,
                    'Bsurf': ini.Bsurf,
                    'Csurf': ini.Csurf,
                    'T': ini.T,
                    'rSh': ini.rSh,
                })

        lbl = f'$B= {device.B: .1e~L}$'
        plttype = axi.plot if plotCQE else axi.semilogy
        xx = device.u.magnitude
        yy = device.cqe if plotCQE else abs(device.i.magnitude)
        plttype (xx, yy, label = lbl, c = colors[ii])

    # axis labels
    axi.set_xlabel(xlbl), axi.set_ylabel(ylbl)
    # limits
    axi.set_xlim(xlims), axi.set_ylim(ylims)
    # legend
    axi.legend(loc='lower right', ncol=1)
    # text
    axi.text(0.02, 0.98, textstr, transform=axi.transAxes, #fontsize=12,
            verticalalignment='top', bbox=props)


    # Effect of C
    # -----------
    axi = ax[0,2]
    # subplot title
    axi.set_title('Effect of $C$')
    # colors
    colors = [ colormap(x) for x in np.linspace(0, 1, len(Cs)) ]
    for ii, C in enumerate(Cs):
        device = funs.LED (**{
                    'chi': 0.6,
                    'radius': radius,
                    'dT': dT,
                    'dM': dM,
                    'uSurfDiode': uSurfDiode,
                    'stack': stack,
                    'ARthickness': ini.ARthickness,
                    'A': ini.A,
                    'B': ini.B,
                    'C': C,
                    'Asurf': ini.Asurf,
                    'Bsurf': ini.Bsurf,
                    'Csurf': ini.Csurf,
                    'T': ini.T,
                    'rSh': ini.rSh,
                })

        lbl = f'$B= {device.B: .1e~L}$'
        plttype = axi.plot if plotCQE else axi.semilogy
        xx = device.u.magnitude
        yy = device.cqe if plotCQE else abs(device.i.magnitude)
        plttype (xx, yy, label = lbl, c = colors[ii])

    # axis labels
    axi.set_xlabel(xlbl), axi.set_ylabel(ylbl)
    # limits
    axi.set_xlim(xlims), axi.set_ylim(ylims)
    # legend
    axi.legend(loc='lower right', ncol=1)
    # text
    axi.text(0.02, 0.98, textstr, transform=axi.transAxes, #fontsize=12,
            verticalalignment='top', bbox=props)


    # Effect of Rspread and Rsurf
    # ---------------------------
    axi = ax[1,1]
    # subplot title
    axi.set_title('Effect of $R_{SF}$ (through $d_T$)')

    idTs = np.linspace (5, 200, 100) * ureg.micron
    idMs = np.linspace (5, 200, 50) * ureg.micron
    # colors
    colors = [ colormap(x) for x in np.linspace(0, 10, len(idTs)) ]

    for ii, dTi in enumerate(idTs):
        for jj, dMi in enumerate(idMs):
            device = funs.LED (**{
                        'chi': 0.6,
                        'radius'      : radius,
                        'dT'          : dTi,
                        'dM'          : dMi,
                        'uSurfDiode'  : uSurfDiode,
                        'stack'       : stack,
                        'ARthickness' : ini.ARthickness,
                        'A'           : ini.A,
                        'B'           : ini.B,
                        'C'           : ini.C,
                        'Asurf'       : ini.Asurf,
                        'Bsurf'       : ini.Bsurf,
                        'Csurf'       : ini.Csurf,
                        'T'           : ini.T,
                        'rSh'         : ini.rSh,
                    })

            if 1.2 > device.rSpread.magnitude > 1.15:
                lbl = f'$R_{{SF}} = {device.rSurf: 4.1f~L} (d_T = {device.dT: .1f~L})$'
                plttype = axi.plot if plotCQE else axi.semilogy
                xx = device.u.magnitude
                yy = device.cqe if plotCQE else abs(device.i.magnitude)
                plttype (xx, yy, label = lbl, c = colors[ii])

    # axis labels
    axi.set_xlabel(xlbl), axi.set_ylabel(ylbl)
    # limits
    axi.set_xlim(xlims), axi.set_ylim(ylims)
    # legend
    axi.legend(loc='lower right', ncol=1)
    # text
    axi.text(0.02, 0.98, textstr, transform=axi.transAxes, #fontsize=12,
            verticalalignment='top', bbox=props)

    # Effect of Asurf
    # ---------------
    axi = ax[1,0]
    # subplot title
    axi.set_title('Effect of $A_{SF}$')
    # colors
    colors = [ colormap(x) for x in np.linspace(0, 1, len(Asurfs)) ]

    for ii, Asurf in enumerate(Asurfs):
        device = funs.LED (**{
                    'chi': 0.6,
                    'radius'      : radius,
                    'dT'          : dTi,
                    'dM'          : dMi,
                    'uSurfDiode'  : uSurfDiode,
                    'stack'       : stack,
                    'ARthickness' : ini.ARthickness,
                    'A'           : ini.A,
                    'B'           : ini.B,
                    'C'           : ini.C,
                    'Asurf'       : Asurf,
                    'Bsurf'       : ini.Bsurf,
                    'Csurf'       : ini.Csurf,
                    'T'           : ini.T,
                    'rSh'         : ini.rSh,
                })

        lbl = f'$A_{{SF}}= {device.Asurf: .1e~L}$'
        plttype = axi.plot if plotCQE else axi.semilogy
        xx = device.u.magnitude
        yy = device.cqe if plotCQE else abs(device.i.magnitude)
        plttype (xx, yy, label = lbl, c = colors[ii])

    # axis labels
    axi.set_xlabel(xlbl), axi.set_ylabel(ylbl)
    # limits
    axi.set_xlim(xlims), axi.set_ylim(ylims)
    # legend
    axi.legend(loc='lower right', ncol=1)
    # text
    axi.text(0.02, 0.98, textstr, transform=axi.transAxes, #fontsize=12,
            verticalalignment='top', bbox=props)

    # Effect of Rspread
    # -----------------
    axi = ax[1,2]
    # subplot title
    axi.set_title('Effect of $R_{SP}$ (through $d_M$)')
    # colors
    colors = [ colormap(x) for x in np.linspace(0, 1, len(dMs)) ]

    for ii, dMi in enumerate(dMs):
        device = funs.LED (**{
                    'chi': 0.6,
                    'radius'      : radius,
                    'dT'          : dT,
                    'dM'          : dMi,
                    'uSFDiode'    : uSurfDiode,
                    'uAR'         : uSurfDiode,
                    'stack'       : stack,
                    'ARthickness' : ini.ARthickness,
                    'A'           : ini.A,
                    'B'           : ini.B,
                    'C'           : ini.C,
                    'Asurf'       : ini.Asurf,
                    'Bsurf'       : ini.Bsurf,
                    'Csurf'       : ini.Csurf,
                    'T'           : ini.T,
                    'rSh'         : ini.rSh,
                })

        lbl = f'$R_{{SP}}= {device.rSpread: .1f~L} (d_M = {device.dM: .1f~L})$'
        plttype = axi.plot if plotCQE else axi.semilogy
        xx = device.u.magnitude
        yy = device.cqe if plotCQE else abs(device.i.magnitude)
        plttype (xx, yy, label = lbl, c = colors[ii])

    # axis labels
    axi.set_xlabel(xlbl), axi.set_ylabel(ylbl)
    # limits
    axi.set_xlim(xlims), axi.set_ylim(ylims)
    # legend
    axi.legend(loc='lower right', ncol=1)
    # text
    axi.text(0.02, 0.98, textstr, transform=axi.transAxes, #fontsize=12,
            verticalalignment='top', bbox=props)


    if fileName:
        fileout = fileName + '-CQE.' + fileFrmt if plotCQE else fileName + '.' + fileFrmt
        plt.savefig(fileout, dpi=350)

    if showPlot:
        fig.set_dpi(150)

    return fig

def PlotDistanceEffect(
        radius, dTs, dMs, uSurfDiode, stack, Asurfs,
        analysis = 'rspread', # analysis='asurf',
        plotCQE=False, savePlot=False,
        colormap = plt.cm.Greens,
        title='', fileName='', fileFrmt='jpg', showPlot=True
    ):
    '''
    Effect of Asurf parameter on the equivalent circuit IV curve
    '''

    # the parameter to be sweeped inside each subplot
    subparam = Asurfs if analysis == 'asurf' else dMs
    # the parameter to be sweeped when switching subplots
    supparam = dMs if analysis == 'asurf' else Asurfs

    # colors
    colors = [ colormap(x) for x in np.linspace(0, 1, len(As)) ]

    nrows = 2
    ncols = 3
    fig, ax = plt.subplots(nrows, ncols)

    title = title + ' (CQE)' if plotCQE else title + ' (IV)'
    fig.suptitle(title)

    # axis labels
    xlbl = '$U$'
    ylbl = '$CQE$' if plotCQE else '$I_L$'
    # axis limits
    xlims = [-0.5, 3]
    ylims = [0.0, 1] if plotCQE else [1e-8, 1]
    # initial properties, text box in subplots
    textstr = '\n'.join((
        f'$Radius   = {radius  : .0g~L}$',
        f'$A        = {ini.A   : .1e~L}$',
        f'$B        = {ini.A   : .1e~L}$',
        f'$C        = {ini.A   : .1e~L}$',
        f'$R_{{SH}} = {ini.rSh : .0f~L}$',
        f'$T        = {ini.T   : .0f~L}$',
        ))
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)

    for ii, g in enumerate(ax):
        for jj, h in enumerate(g):
            axi = h
            supVal = supparam [jj + ncols * ii]
            for kk, subVal in enumerate(subparam):
                device = funs.LED (**{
                        'chi': 0.6,
                        'dM': supVal if analysis=='asurf' else subVal if analysis=='rspread' else 0,
                        'Asurf': subVal if analysis=='asurf' else supVal if analysis=='rspread' else 0,
                        'radius': radius, 'dT': dT,
                        'uSurfDiode': uSurfDiode, 'stack': ledStack,
                        'ARthickness': ini.ARthickness,
                        'A': ini.A, 'B': ini.B, 'C': ini.C,
                        'Bsurf': ini.Bsurf, 'Csurf': ini.Csurf,
                        'T': ini.T, 'rSh': ini.rSh
                    })

                xx = device.u
                yy = device.cqe if plotCQE else abs(device.i)
                plttype = axi.plot if plotCQE else axi.semilogy
                #  lbl = f'$R_{{SF}} = {device.rSurf: .1f~L}$'
                lbl = f'$R_{{SP}} = {device.rSpread: .1f~L}, A_{{SF}} = {device.Asurf: .1e~L}$'
                plttype(xx, yy, label=lbl, c = colors[kk])

                axi.set_title (f'$R_{{SP}} = {device.rSpread: 0.1f~L}, d_T = {device.dT: 0.1f~L}, d_M = {device.dM: 0.1f~L}$')
            axi.set_xlabel(xlbl)
            axi.set_ylabel(ylbl)
            axi.set_xlim(xlims)
            axi.set_ylim(ylims)
            axi.legend(loc='lower right', ncol=1)
            axi.text(0.02, 0.98, textstr, transform=axi.transAxes, #fontsize=12,
                    verticalalignment='top', bbox=props)

    fileout = fileName + '-CQE.' + fileFrmt if plotCQE else \
            fileName + '-IV.' + fileFrmt

    if savePlot:
        plt.savefig(fileout, format=fileFrmt, dpi=350)

    if showPlot:
        fig.set_dpi(150)


########
# Plots
########

#  # Contribution of each component to the total current
us = np.linspace (0, 5, 5000) * ureg.V
radius = Q_(125, 'microns')
dT = Q_(95, 'microns')
dM = Q_(95, 'microns')

device = funs.LED (**{
            'chi'         : ini.chi,
            'radius'      : radius,
            'dT'          : dT,
            'dM'          : dM,
            'uSFDiode'    : us,
            'uAR'         : us,
            'stack'       : ledStack,
            'ARthickness' : ini.ARthickness,
            'A'           : ini.A,
            'B'           : ini.B,
            'C'           : ini.C,
            'Asurf'       : ini.Asurf,
            'Bsurf'       : ini.Bsurf,
            'Csurf'       : ini.Csurf,
            'T'           : ini.T,
            'rSh'         : ini.rSh,
            'i0SF'        : ini.i0SF,
            'ARNdio'      : ini.ARNdio,
        })

PlotECcomponents(deviceList, device, savePlot=True, fileName='output/currentComponents-250micron', fileFrmt='jpg')

#  # Effect of each parameter to the total current
As = 3 * np.logspace (2, 9, 10) * ureg('s**-1')
Bs = 3 * np.logspace (-2, -12, 10) * ureg('cm**3 / s')
Cs = 3 * np.logspace (-15, -42, 10) * ureg.cm**6 / ureg.second
Asurfs = 3 * np.logspace (2, 13, 10) / ureg.second
dTs = np.linspace (5, 200, 10) * ureg.micron
dMs = np.linspace (5, 200, 10) * ureg.micron

#  PlotParamsEffect(
#          radius, dT, dM, us, ledStack, As, Bs, Cs, Asurfs, dTs, dMs,
#          plotCQE=True, savePlot=True,
#          title='Parameters effect of ABC formula',
#          fileName='output/parametersEffectABC', fileFrmt = 'jpg',
#      )


# Effect of Asurf or Rspread
Asurfs = 3 * np.logspace (2, 13, 10)
Asurfs = 3 * np.logspace (5, 14, 6) / ureg.second
#  dTs = np.linspace (5, 200, 6) * ureg.micron
dTs = [5, 10, 20, 40, 50, 75, 95] * ureg.micron
dMs = [5, 10, 20, 40, 50, 75, 95] * ureg.micron

#  PlotDistanceEffect (
#          radius, dTs, dMs, us, ledStack, Asurfs,
#          analysis = 'rspread',
#          plotCQE=False, savePlot=False,
#          title='Asurf', fileName='', fileFrmt='jpg', showPlot=True
#      )

plt.show()
