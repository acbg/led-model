##############################
# Import packages and modules
##############################
from time import time
stime = time()
print(f'Elapsed {time() - stime:.3f} s: Initialise, load modules')
import warnings
import numpy as np
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from pprint import pprint

import sys
# access to all other packages in the upper directory
sys.path.append('../')
from physics.constants import ureg, Q_
import physics.constants as pc

import functions as funs
import initVals as ini
from stack import ledStack


###########################
# Modules/packages options
###########################
warnings.filterwarnings("ignore")

np.set_printoptions(threshold=np.inf)

#  pd.set_option('display.max_rows', 500)
#  pd.set_option('display.max_columns', 500)
#  pd.set_option('display.width', 0)

#  plt.style.use('bmh')
#  plt.style.use('tableau-colorblind10')
#  plt.style.use('fivethirtyeight')
#  plt.style.use('seaborn-darkgrid')
#  plt.style.use('dark_background')
plt.style.use('ggplot')

plt.rcParams.update({
    'text.usetex'       : True,
    'axes.edgecolor'    : 'dimgrey',
    'axes.grid'         : False,
    'font.size'         : 6.0,
    'lines.linewidth'   : 1.0,
    'lines.markersize'  : 1.0,
    'legend.shadow'     : False,
    'legend.fancybox'   : False,
    })

################################################
print(f'Elapsed {time() - stime:.3f} s: Plots')
################################################

#  uSFDiode = np.linspace (-1.5, 3, 8000) * ureg.volt

#  pizzaDev = funs.pizza (**{
#                      'radius'     : 10 * ureg.micron,
#                      'dT'         : 2 * ureg.micron,
#                      'dM'         : 2 * ureg.micron,
#                      'holeRad'    : 10 * ureg.micron,
#                      'holeSep'    : 50 * ureg.micron,
#                      'FF'         : 0.1,
#                      'uSurfDiode' : uSFDiode,
#                      'stack'      : ledStack,
#                      'ARthickness': ini.ARthickness,
#                      'A'          : ini.A,
#                      'B'          : ini.B,
#                      'C'          : ini.C,
#                      'Asurf'      : ini.Asurf,
#                      'Bsurf'      : ini.Bsurf,
#                      'Csurf'      : ini.Csurf,
#                      'T'          : ini.T,
#                      'rSh'        : ini.rSh
#                      }
#                  )


#  plt.figure()
#  plt.semilogy (pizzaDev.u, pizzaDev.i, label=f'$N_{{LEDs}}={pizzaDev.nHoles:.0e}$')
#  plt.legend()
#  plt.xlim([-1,4])
#  plt.ylim([1e-9,1e2])
#  plt.show()


# Parameters
uSFDiode = np.linspace (-1.5, 3, 3000) * ureg.volt

fixParams = {
        'radius'     : 1000*ureg.micron,
        'dT'         : 10*ureg.micron,
        'dM'         : 10*ureg.micron,
        'holeRad'    : 30*ureg.micron,
        'FF'         : 0.1,
        'uSurfDiode' : uSFDiode,
        'stack'      : ledStack,
        'ARthickness': ini.ARthickness,
        'A'          : ini.A,
        'B'          : ini.B,
        'C'          : ini.C,
        'Asurf'      : ini.Asurf,
        'Bsurf'      : ini.Bsurf,
        'Csurf'      : ini.Csurf,
        'T'          : ini.T,
        'rSh'        : ini.rSh
    }

colormap = plt.cm.Spectral

# Effect of radius
###################
rads = np.array([250, 500, 1000, 2000, 3000]) * ureg.microns
rads = np.linspace (250, 2000, 8) * ureg.micron
colors = [ colormap(x) for x in np.linspace(0, 1, len(rads)) ]

fig = plt.figure()
fig.suptitle (f'Effect of radius')

for ii, value in enumerate(rads):

    params={}; params.update(fixParams); params.update ({'radius': value})
    dev = funs.pizza (**params)
    lbl = f'$r={dev.radius:.0f~L},' + f'$n_H = {dev.nHoles.magnitude:.0f}$'
    plt.semilogy ( dev.hole.u, abs(dev.hole.i), '--', c=colors[ii])#, label=lbl + ' (single diode)',
    plt.semilogy ( dev.u, abs(dev.i), c=colors[ii], label=lbl,)
    plt.xlim([-1,5])
    plt.ylim([1e-7,1e5])
    plt.legend(loc='best', ncol=1)
    #  print(f'Elapsed {time() - stime:.3f} s: New curve')
#  plt.show()

# Effect of dT
###############
dTs = np.array([5, 10, 15, 20, 30, 40]) * ureg.microns
dTs = np.linspace (5, 40, 25) * ureg.micron
colors = [ colormap(x) for x in np.linspace(0, 1, len(dTs)) ]

fig = plt.figure()
fig.suptitle (f'Effect of $d_T$')
for ii, value in enumerate(dTs):
    params={}; params.update(fixParams); params.update ({'dT': value})
    dev = funs.pizza (**params)
    lbl=f'$d_T={dev.hole.dT:.0f~L}, n_H = {dev.nHoles.magnitude:.0f}$'
    plt.semilogy ( dev.hole.u, abs(dev.hole.i), '--', c=colors[ii])#, label=lbl + ' (single diode)',)
    plt.semilogy ( dev.u, abs(dev.i), c=colors[ii], label=lbl,)
    plt.xlim([-1,5])
    plt.ylim([1e-7,1e5])
    plt.legend(loc='best', ncol=1)
    #  print(f'Elapsed {time() - stime:.3f} s: New curve')
#  plt.show()

# Effect of dM
###############
dMs = np.array([5, 10, 15, 20, 30, 40]) * ureg.microns
dMs = np.linspace (5, 40, 25) * ureg.micron
colors = [ colormap(x) for x in np.linspace(0, 1, len(dMs)) ]

fig = plt.figure()
fig.suptitle (f'Effect of $d_M$')
for ii, value in enumerate(dMs):
    params={}; params.update(fixParams); params.update ({'dM': value})
    dev = funs.pizza (**params)
    lbl=f'$d_M={dev.hole.dM:.0f~L}, n_H = {dev.nHoles.magnitude:.0f}$'
    plt.semilogy ( dev.hole.u, abs(dev.hole.i), '--', c=colors[ii])#, label=lbl + ' (single diode)',)
    plt.semilogy ( dev.u, abs(dev.i), c=colors[ii], label=lbl,)
    plt.xlim([-1,5])
    plt.ylim([1e-7,1e5])
    plt.legend(loc='best', ncol=1)
    #  print(f'Elapsed {time() - stime:.3f} s: New curve')

# Effect of holeRad
####################
holeRads = np.array([10, 20, 30, 40, 50, 60, 70, 80]) * ureg.microns
holeRads = np.linspace (1, 100, 50) * ureg.micron
colors = [ colormap(x) for x in np.linspace(0, 1, len(holeRads)) ]

fig = plt.figure()
fig.suptitle (f'Effect of $d_M$')
for ii, value in enumerate(holeRads):
    params={}; params.update(fixParams); params.update ({'holeRad': value})
    dev = funs.pizza (**params)
    lbl=f'$r_H={dev.holeRad:.0f~L}, n_H = {dev.nHoles.magnitude:.0f}, FF = {dev.FF:.0f}$'
    plt.semilogy ( dev.hole.u, abs(dev.hole.i), '--', c=colors[ii])#, label=lbl + ' (single diode)',)
    plt.semilogy ( dev.u, abs(dev.i), c=colors[ii], label=lbl,)
    plt.xlim([-1,5])
    plt.ylim([1e-7,1e5])
    plt.legend(loc='best', ncol=1)
    #  print(f'Elapsed {time() - stime:.3f} s: New curve')

# Effect of FF
###############
FFs = np.array([0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80])
FFs = np.linspace (0, 1, 50)
colors = [ colormap(x) for x in np.linspace(0, 1, len(FFs)) ]

fig = plt.figure()
fig.suptitle (f'Effect of $FF$')
for ii, value in enumerate(FFs):
    params={}; params.update(fixParams); params.update ({'FF': value})
    dev = funs.pizza (**params)
    lbl=f'$FF={dev.FF:.2f}, n_H = {dev.nHoles.magnitude:.0f}, r_H = {dev.holeRad:.0f~L}$'
    plt.semilogy ( dev.hole.u, abs(dev.hole.i), '--', c=colors[ii])#, label=lbl + ' (single diode)',)
    plt.semilogy ( dev.u, abs(dev.i), c=colors[ii], label=lbl,)
    plt.xlim([-1,5])
    plt.ylim([1e-7,1e5])
    plt.legend(loc='best', ncol=1)
    #  print(f'Elapsed {time() - stime:.3f} s: New curve')

plt.show()

#######################################

rads = np.array([1000, 2000]) * ureg.microns
dTs = np.array([5, 10, 20, 40]) * ureg.microns
dMs = np.array([5, 10, 20, 40]) * ureg.microns
holeRads = np.array([30, 60, 90]) * ureg.microns
FFs = np.array([0.01, 0.05, 0.20, 0.50, 1.00])

colormap = plt.cm.Spectral
colors = [ colormap(x) for x in np.linspace(0, 1, len(FFs)) ]
lines = ['-', '--', '-.', ':']


#  for rad in rads[[1]]:
for rad in rads:
    fig, ax = plt.subplots(len(dTs), len(dMs))
    fig.suptitle (f'$ r = {rad:.0f~L} $')
    for ii, dT in enumerate(dTs):
        for jj, dM in enumerate(dMs):
            for kk, holeRad in enumerate(holeRads):
                for ll, FF in enumerate(FFs):
                    fixParams.update ({'dT': dT, 'dM': dM, 'holeRad': holeRad, 'FF': FF})
                    dev = funs.pizza (**fixParams)

                    lbl=f'$r_H={dev.holeRad:.0f~L}, FF={dev.FF:.2f}$'
                    #  ax[ii][jj].semilogy (
                    #          dev.hole.u, abs(dev.hole.i), '--',
                    #          #  ls = lines[kk],
                    #          c=colors[ll],
                    #          label=lbl,
                    #      )
                    ax[ii][jj].semilogy (
                            dev.u, abs(dev.i),
                            #  ls = lines[kk],
                            c=colors[ll],
                            label=lbl,
                        )

                    ax[ii][jj].set_title(f'$d_T={dev.hole.dT:.1f~L}, d_M={dev.hole.dM:.1f~L}$')
                    ax[ii][jj].set_xlim([-1,5])
                    ax[ii][jj].set_ylim([1e-7,1e5])
                    ax[ii][jj].legend(loc='best', ncol=1)
                    #  print(f'Elapsed {time() - stime:.3f} s: New curve')

print(f'Elapsed {time() - stime:.3f} s: All plotted')
plt.show ()
