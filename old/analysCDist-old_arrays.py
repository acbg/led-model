# TODO: gui with sliders to visualize effect of the different parameters on the IV curve
import warnings
import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
import matplotlib as mpl
import matplotlib.pyplot as plt
from pprint import pprint
import functions as funs
from tabulate import tabulate # print nice tables
import initVals as initial

warnings.filterwarnings("ignore")
np.set_printoptions(threshold=np.inf)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 0)
#  plt.style.use('bmh')
#  plt.style.use('tableau-colorblind10')
#  plt.style.use('fivethirtyeight')
#  plt.style.use('ggplot')
plt.style.use('seaborn-darkgrid')
#  plt.style.use('dark_background')
mpl.rcParams.update({
    'text.usetex'       : True,
    'font.size'         :    4,
    'lines.linewidth'   :  1.0,
    'lines.markersize'  :  1.0,
    })


############
# Constants
############
#  q, k = np.genfromtxt('constants.csv')

def PlotRadiusEffect(uSurfDiode, fun, stack, rads, dTs, dMs, colormap=plt.cm.Spectral, savePlot=False, saveDir='./output/', fileFrmt='jpg', showPlot=True, title='Effect of Radius (for dT and dM)'):
    fig, ax = plt.subplots(3,7)
    fig.suptitle(title)
    #  fig.tight_layout()
    ax0 = ax[0]
    ax1 = ax[1]
    ax2 = ax[2]
    colors = [ colormap(x) for x in np.linspace(0, 1, len(rads)) ]
    for ii, (dT, dM) in enumerate(zip(dTs, dMs)):
        #  jj = ii if ii < 6 else ii-6
        cqeg = []
        for jj, radius in enumerate(rads):
            area = radius * np.pi**2
            rSpread = funs.rCSL (stack, radius, dT, dM)
            rSurf = funs.rSF (stack, radius, dT)
            outVars = fun (
                    uSurfDiode,
                    initial.A, initial.B, initial.C, initial.Asurf, initial.Bsurf, initial.Csurf,
                    rSpread, rSurf,
                    initial.rSh, initial.ARthickness, area, initial.T)
            u = outVars['u']; i = outVars['i']; cqe = outVars['cqe']; iLrad = outVars['iLrad']

            # Interpolate u and cqe so there is a value at, e.g., u=1.2
            uVals = np.arange(-1.5, 3, 0.05)
            uInterp = np.interp (uVals, u, u)
            cqeInterp = np.interp (uVals, u, cqe)
            ax0[ii].semilogy(u, abs(iLrad), '-', c=colors[jj], label=(r'$radius = %.0f \mu$m' %(1e6*radius)))
            ax1[ii].semilogy (u, abs(i), '-', c=colors[jj], label=(r'$radius = %.0f \mu$m' %(1e6*radius)))
            #  ax1[ii].plot (uInterp, cqeInterp, '-', c=colors[jj], label=(r'$radius = %.0f \mu$m' %(1e6*radius)))

            cqei = cqeInterp[np.round(uInterp,2)==1.2][0]
            cqeg.append (cqei)

        f2 = interp1d(rads, cqeg, kind='linear')
        rads2 = np.linspace(min(rads), max(rads), 100)

        ax2[ii].plot (rads, cqeg, 's', rads2, f2(rads2), '-')


        ax0[ii].set_xlabel('$U_1$')
        ax0[ii].set_ylabel('$I_{RAD}$')
        ax0[ii].set_xlim([-0.5,3])
        ax0[ii].set_ylim([1e-8,10])
        textstr = '\n'.join((
            r'$d_{T}=%.0f \mu$m' % (1e6*dT),
            r'$d_{M}=%.0f \mu$m' % (1e6*dM),
            r'$A=%.1e$' % (initial.A),
            r'$B=%.1e$' % (initial.B),
            r'$C=%.1e$' % (initial.C),
            r'$A_{surf}=%.1e$' % (initial.Asurf),
            r'$B_{surf}=%.1e$' % (initial.Bsurf),
            r'$C_{surf}=%.1e$' % (initial.Csurf),
            r'$R_{shunt}=%.1e$' % (initial.rSh),
            r'$ARthickness=%.1e$' % (initial.ARthickness),
            ))
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        ax0[ii].text(0.98, 0.02, textstr, transform=ax0[ii].transAxes, #fontsize=12,
                verticalalignment='bottom', horizontalalignment='right', bbox=props)

        ax1[ii].set_xlabel('$U_1$')
        ax1[ii].set_ylabel('$I_1$')
        ax1[ii].set_xlim([-0.5,3])
        ax1[ii].set_ylim([1e-8,10])
        #  ax1[ii].set_xlim(left=0)
        #  ax1[ii].set_ylim([0.99,1.00])
        #  ax1[ii].legend(loc='best')

        ax2[ii].set_xlabel('$diameter$')
        ax2[ii].set_ylabel('$CQE (U=1.2 V)$')

    if savePlot:
        plt.savefig(saveDir+'effectRadius.'+fileFrmt, dpi=150)

    if showPlot:
        fig.set_dpi(150)

    return fig

def PlotDistancesAndR (uSurfDiode, fun, initVals, stack, rads, dTs, dMs, savePlot=False, saveDir='./output/', fileFrmt='jpg', showPlot=True):
    '''
    Effect of the distance dM (between top mesa and middle contact
    '''
    ARthickness, A, B, C, Asurf, Bsurf, Csurf, rSh, rSurf, rSpread, i0S, i0L, i0P, T = initVals
    for radius in rads:
        area = np.pi * radius**2
        fig, ax = plt.subplots(1, 2)
        #  fig.tight_layout()
        fig.suptitle('Combined effect of $d_T$ and $d_M$ ($Radius = %.0f \mu$m)' %(1e6*radius))
        for ii, (dT, dM) in enumerate(zip(dTs, dMs)):
            ax0 = ax[0]
            ax1 = ax[1]
            rSurf = funs.rSF (stack, radius, dT)
            rSpread = funs.rCSL (stack, radius, dT, dM)
            outVars = fun (uSurfDiode, A, B, C, Asurf, Bsurf, Csurf, rSpread, rSurf, rSh, ARthickness, area, T)
            u = outVars['u']; i = outVars['i']; cqe = outVars['cqe']
            ax0.semilogy(u, abs(i), '-')
            ax0.set_xlabel('$U_1$'), ax0.set_ylabel('$I$')
            ax0.set_xlim([-0.0,3]), ax0.set_ylim([1e-5,1e2])
            textstr = '\n'.join((
                r'$A=%.1e$' % (A),
                r'$B=%.1e$' % (B),
                r'$C=%.1e$' % (C),
                r'$A_{surf}=%.1e$' % (Asurf),
                ))
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            ax0.text(0.60, 0.20, textstr, transform=ax0.transAxes, #fontsize=12,
                    verticalalignment='top', bbox=props)

            ax1.plot(u, cqe, '-', \
                    label=(r'$d_T = %2.0f \mu$m, $d_M = %2.0f \mu$m, $R_{SP} = %3.2f \Omega/sq$, $R_{SF} = %3.0f \Omega/sq$'%(1e6*dM, 1e6*dM, rSpread, rSurf)))
            ax1.set_xlabel('$U_1$'), ax1.set_ylabel('$CQE$')
            ax1.set_xlim([1.0,2.0]), ax1.set_ylim([0.98,1.0])
            ax1.legend(loc='best', ncol=1)
        if savePlot:
            plt.savefig(saveDir + 'effectDistanceAndR-%04.0i.'%(1e6*radius) + fileFrmt , dpi=150)
        if showPlot:
            fig.set_dpi(150)

    return fig

def PlotdMEffect (uSurfDiode, fun, initVals, stack, rads, dTs, dMs, savePlot=False, saveDir='./output/', fileFrmt='jpg', showPlot=True):
    '''
    Effect of the distance dM (between top mesa and middle contact
    '''
    ARthickness, A, B, C, Asurf, Bsurf, Csurf, rSh, rSurf, rSpread, i0S, i0L, i0P, T = initVals
    for radius in rads:
        area = np.pi * radius**2
        fig, ax = plt.subplots(2, 4)
        #  fig.tight_layout()
        fig.suptitle('Effect of $d_M (Radius = %.0f \mu$m)' %(1e6*radius))
        for ii, dT in enumerate(dTs):
            ax0 = ax[0,ii]
            ax1 = ax[1,ii]
            for dM in dMs:
                rSurf = funs.rSF (stack, radius, dT)
                rSpread = funs.rCSL (stack, radius, dT, dM)
                outVars = fun (uSurfDiode, A, B, C, Asurf, Bsurf, Csurf, rSpread, rSurf, rSh, ARthickness, area, T)
                u = outVars['u']; i = outVars['i']; cqe = outVars['cqe']
                ax0.semilogy(u, abs(i), '-')
                ax1.plot(u, cqe, '-', \
                        label=(r'$d_M = %3.0f \mu$m'%(1e6*dM)))
            ax0.set_xlabel('$U_1$'), ax0.set_ylabel('$I$')
            ax0.set_xlim([-0.0,4]), ax0.set_ylim([1e-5,10])
            textstr = '\n'.join((
                r'$d_{T}=%.0f \mu$m' % (1e6*dT),
                r'$A=%.1e$' % (A),
                r'$B=%.1e$' % (B),
                r'$C=%.1e$' % (C),
                r'$A_{surf}=%.1e$' % (Asurf),
                ))
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            ax0.text(0.60, 0.20, textstr, transform=ax0.transAxes, #fontsize=12,
                    verticalalignment='top', bbox=props)
            ax1.set_xlabel('$U_1$'), ax1.set_ylabel('$CQE$')
            ax1.set_xlim([-0.1,4]), ax1.set_ylim([0,1])
            ax1.legend(loc='best', ncol=1)
        if savePlot:
            plt.savefig(saveDir + 'effectdM-%04.0i.'%(1e6*radius) + fileFrmt , dpi=150)
        if showPlot:
            fig.set_dpi(150)
    return fig

def PlotdTEffect (uSurfDiode, fun, initVals, stack, rads, dTs, dMs, savePlot=False, saveDir='./output/', fileFrmt='jpg', showPlot=True):
    '''
    Effect of the distance dT (between top mesa and top contact
    '''
    for radius in rads:
        area = np.pi * radius**2
        fig, ax = plt.subplots(2, 4)
        #  fig.tight_layout()
        fig.suptitle('Effect of $d_T (Radius = %.0f \mu$m)' %(1e6*radius))
        ARthickness, A, B, C, Asurf, Bsurf, Csurf, rSh, rSurf, rSpread, i0S, i0L, i0P, T = initVals
        for ii, dM in enumerate(dMs):
            ax0 = ax[0,ii]
            ax1 = ax[1,ii]
            for dT in dTs:
                rSurf = funs.rSF (ledStack, radius, dT)
                rSpread = funs.rCSL (ledStack, radius, dT, dM)
                outVars = fun (uSurfDiode, A, B, C, Asurf, Bsurf, Csurf, rSpread, rSurf, rSh, ARthickness, area, T)
                u, i, cqe = outVars['u'], outVars['i'], outVars['cqe']
                ax0.semilogy(u, abs(i), '-', label=(r'$d_T = %.0f \mu$m' %(1e6*dT)))
                ax1.plot(u, cqe, '-', label=(r'$d_T = %.0f \mu$m' %(1e6*dT)))
            ax0.set_xlabel('$U_1$'), ax0.set_ylabel('$I$')
            ax0.set_xlim([-0.0,3]), ax0.set_ylim([1e-5,10])
            textstr = '\n'.join((
                r'$d_{M}=%.0f \mu$m' % (1e6*dM),
                r'$A=%.1e$' % (A),
                r'$B=%.1e$' % (B),
                r'$C=%.1e$' % (C),
                r'$A_{surf}=%.1e$' % (Asurf),
                ))
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            ax0.text(0.60, 0.20, textstr, transform=ax0.transAxes, #fontsize=12,
                    verticalalignment='top', bbox=props)
            ax1.set_xlabel('$U_1$'), ax1.set_ylabel('$CQE$')
            ax1.set_xlim([-0.1,4]), ax1.set_ylim([0,1])
            ax1.legend(loc='best', ncol=1)

        if savePlot:
            plt.savefig(saveDir + 'effectdT-%04.0i.' %(1e6*radius) + fileFrmt, dpi=150)
        if showPlot:
            fig.set_dpi(150)


#####################################################################
# DDS model
# including a surface diode and surface and spreading resistance
#####################################################################


##############################
# Effect of contact distances
##############################

# Define the DDS stack layers
# ---------------------------
# Read layers properties from file
stack = pd.read_csv('stack.csv', delimiter=',\t+', comment='#')

# Create dataframe containing the layers
df = pd.DataFrame(stack)

# Set SI units
df.Doping = 1e6*df.Doping
df.Mobility = 1e-4*df.Mobility
df.Thickness = 1e-9*df.Thickness

# Reorder columns
df = df[['Name', 'Material', 'dopingType', 'Mobility', 'Doping', 'Thickness']]

# Define LED layers
ledTC  = funs.layer(*df[df.Name.str.contains('Top Contact')].values.T)
ledPB1 = funs.layer(*df[df.Name.str.contains('p-barrier 1')].values.T)
ledPB2 = funs.layer(*df[df.Name.str.contains('p-barrier 2')].values.T)
ledPB3 = funs.layer(*df[df.Name.str.contains('p-barrier 3')].values.T)
ledPB4 = funs.layer(*df[df.Name.str.contains('p-barrier 4')].values.T)
ledAR  = funs.layer(*df[df.Name.str.contains('LED AR')].values.T)
ledAR.isAR = 1
#  ledAR.thickness = 1e-9*np.linspace(1,10,10)
ledNB  = funs.layer(*df[df.Name.str.contains('n-barrier')].values.T)
ledMC  = funs.layer(*df[df.Name.str.contains('Middle Contact')].values.T)
ledMC.isSpreading = 1
ledCSL = funs.layer(*df[df.Name.str.contains('CSL')].values.T)
ledCSL.isSpreading = 1
ledStack = [ledTC, ledPB1, ledPB2, ledPB3, ledPB4, ledAR, ledNB, ledMC, ledCSL]

# #####
# Plots
# #####

# Surface diode voltage
uSurfD = np.linspace (-1.5, 3, 5000)
# Initial values for the simulations
#  initVals = np.genfromtxt('initialValues.csv', delimiter=',')

rads = 1e-6 * np.arange(100, 2000, 50)
rads = 1e-6 * np.array([125, 250, 500, 1000, 1250, 1500, 1750, 2000])
rads = 1e-6 * np.linspace(100, 2000, 50)

# Effect on the resistances
# -------------------------
#  dTs = [5, 10, 20, 40, 50, 75, 95] * 1e-6
#  dMs = [5, 10, 20, 40, [50, 25], [25, 75], [5, 95]] * 1e-6
dTs = np.linspace(5, 200, 50) * 1e-6
dMs = np.linspace(5, 200, 50) * 1e-6

# Table of rSurf and rspread dependence on dT and dM
#  data = []
#  for rad in rads:
#      for dM in dMs:
#          for dT in dTs:
#              rSpread = funs.rCSL (ledStack, rad, dT, dM)
#              rSurf = funs.rSF (ledStack, rad, dT)
#              data.append([rad, dT, dM, rSurf, rSpread])
#
#  df = pd.DataFrame(data, columns=['radius', 'dT', 'dM', 'rSurf', 'rSpread'])
#  table = tabulate(df, headers='keys', tablefmt='github')
#  df.to_csv('output/distanceAndResistances.csv', index=False)
#  df.to_html('output/distanceAndResistances.html', index=False)
#  print(df)
#  f = open('output/distanceEffectOnResistances', 'w')
#  f.write(tabulate(df, headers='keys', tablefmt='github'))
#  f.close()


a = np.linspace(5, 200, 4) * 1e-6
b = np.linspace(5, 200, 10) * 1e-6

# Effect of dT and dM
# -------------------
dTs, dMs = a, b
#  PlotdMEffect (uSurfD, funs.ledEC, initVals, ledStack, rads, dTs, dMs, savePlot=False)

dTs, dMs = b, a
#  PlotdTEffect (uSurfD, funs.ledEC, initVals, ledStack, rads, dTs, dMs, savePlot=False)

##########################################
dTs = 1e-6 * np.array ([5, 10, 20, 40, 50, 75, 95])
dMs = 1e-6 * np.array ([5, 10, 20, 40, 50, 75, 95])
#  dTs = 1e-6 * np.linspace (5, 200, 7)
#  dMs = 1e-6 * np.linspace (5, 200, 7)

# Effect of dT and dM on Rspread and Rsurf
# ----------------------------------------
uSurfD = np.linspace (-1.5, 3, 5000)
PlotDistancesAndR (uSurfD, funs.ledEC, initVals, ledStack, rads, dTs, dMs, savePlot=False)

# Effect of radius
# ----------------
#  PlotRadiusEffect(uSurfD, funs.ledEC, ledStack, rads, dTs, dMs, savePlot=True)

# Show plots
plt.show()
###########
