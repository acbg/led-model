import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 0)
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import constantsTdep

q, k = np.genfromtxt('constants.csv')


##########################################################################
# Functions
##########################################################################
def layerR (mobility, carrier_concent, length, crossSection):
    '''
    resistance of a thin film layer
    '''
    conductivity = q * carrier_concent * mobility
    resistivity = 1 / conductivity
    resistance = resistivity * length / crossSection
    return resistance, conductivity, resistivity

def rSurf (stack, radius, dT):
    rSurf = 0
    for layer in stack:
        mobility = layer.mobility
        doping=layer.doping
        crossSect = layer.thickness * 2 * np.pi * radius
        rSurf = rSurf + layerR (mobility, doping, dT, crossSect)[0] if layer.isAR else rSurf
    return rSurf

def rSpread (stack, radius, dT, dM):
    rSpread = 0
    for layer in stack:
        mobility = layer.mobility
        doping=layer.doping
        crossSect = layer.thickness * 2 * np.pi * radius
        rSpread = rSpread + layerR (mobility, doping, dT + dM, crossSect)[0] if layer.isSpreading else rSpread
    return rSpread

class layer:
    def __init__(self, name, material, dopingType, mobility, doping, thickness, isLateral=0, isAR=0, isSpreading=0, radius=1, dT=1, dM=1):
        self.name = name
        self.material = material
        self.dopingType = dopingType
        self.isLateral = isLateral # does the layer have lateral conduction?
        self.isAR = isAR # is the layer the AR?
        self.isSpreading = isSpreading # is it a spreading layer?
        self.mobility = mobility
        self.doping = doping
        self.thickness = thickness
        self.radius = radius
        self.dT = dT
        self.dM = dM
        self.dC = dT + dM

    def crossSectLat(self):
        return self.thickness * 2 * np.pi * self.radius

    def chanLenLat(self):
        return self.dT + self.dM

    def rLat(self):
        rLat = layerR(self.mobility, self.doping, self.dM, self.crossSectLat())[0] if (self.isSpreading or self.isLateral) \
                else layerR(self.mobility, self.doping, self.dT, self.crossSectLat())[0] if self.isAR \
                else np.nan
        return rLat

    def crossSectTrans(self):
        return np.pi * self.radius**2

    def chanLenTrans(self):
        return self.thickness

    def rTrans(self):
        return layerR(self.mobility, self.doping, self.chanLenTrans(), self.crossSectTrans())[0]

    def r(self):
        return self.rLat() if self.isLateral else self.rTrans()

    def summary(self):
        #  summary = [self.name, self.material, self.isLateral, self.dopingType, self.mobility, self.thickness, self.radius, self.dT, self.dM, self.rLat(), self.rTrans()]
        summary = pd.DataFrame(np.vstack([self.name, self.material, self.dopingType, self.isLateral, self.isAR, self.isSpreading, self.mobility, self.doping, self.thickness, self.radius, self.dT, self.dM,  self.rLat(), self.rTrans(), self.r()]).T,
                columns = ['Name', 'Material', 'Type', 'isLateral', 'isAR', 'isSpread', 'Mobility', 'Doping', 'Thickness', 'Radius', 'dT', 'dM', 'rLat', 'rTrans', 'r']
                )
        return summary

class stack:
    #TODO: define stack layers so that one can calculate the resistance of the stack
    def __init__(self, stack, radius=1, dT=1, dM=1):
        self.stack = stack
        self.radius = radius
        self.dT = dT
        self.dM = dM
        for layer in stack:
            layer.radius = self.radius
            layer.dT = self.dT
            layer.dM = self.dM

    def rSurf(self):
        rSurf = 0
        for layer in self.stack:
            rSurf = rSurf + layer.rLat() if layer.isAR else rSurf
        return rSurf

    def rSpread(self):
        rSpread = 0
        for layer in self.stack:
            rSpread = rSpread + layer.rLat() if layer.isSpreading else rSpread
        return rSpread

    def rTrans(self):
        rTrans = 0
        for layer in self.stack:
            rTrans =  rTrans + layer.rTrans() if not layer.isLateral() else rTrans
        return rTrans

    #  def rLat(self):
    #      rLat = 0
    #      for layer in self.stack:
    #          rLat =  rLat + layer.rLat() if layer.isLateral() else rLat
    #      return rLat

    def resistance(self):
        rs=0
        for layer in self.stack:
            rs =  rs + layer.r() if not layer.isAR else rs
        rs = rs + self.rSpread()
        return rs

    def layerNames(self):
        names = np.array([])
        for layer in self.stack:
            names = np.append(names,layer.name)
        return names

    def summary(self):
        summary = pd.DataFrame()
        for layer in self.stack:
            summary = pd.concat([summary, layer.summary()])
        stacksum = pd.DataFrame(np.vstack(['Whole Stack', self.radius, self.dT, self.dM,  self.resistance(), self.rSurf(), self.rSpread()]).T,
                columns = ['Name', 'Radius', 'dT', 'dM', 'r', 'rSurf', 'rSpread']
                )
        summary = pd.concat([summary, stacksum])
        #  summary.index = summary.Name
        return summary

#############################################################

'''
radius = 1000e-6 # [m]
area = np.pi * radius**2
dC = np.linspace(5e-6,190e-6,100)

ledStack  = {
        'layer'       : ['Top Contact', 'p-barrier 1', 'p-barrier 2', 'p-barrier 3', 'e-block/p-barrier 4',   'AR', 'n-barrier', 'Middle Contact', 'h-block/CSL'],
        'material'    : [       'GaAs',      'AlGaAs',      'AlGaAs',       'GaInP',               'GaInP', 'GaAs',     'GaInP',           'GaAs',       'GaInP'],
        'type'        : [          'p',           'p',           'p',           'p',                   'p',    'i',         'n',              'n',           'n'],
        'lateral'     : [            0,             0,             0,             0,                     0,      0,           0,                1,             1],
        'mobility'    : [          400,           594,           594,            35,                    35,    0.6,        1000,             8500,           500],# [cm2/Vs]
        'doping'      : [         2e19,          2e19,          4e17,          2e18,                  4e17, 2.1e16,        2e17,             8e18,          8e18],# [cm-3]
        'crossSect'   : [         area,          area,          area,          area,                  area,   area,        area,     20e-9*2*np.pi*radius, 810e-9*2*np.pi*radius],# [m2]
        'thickness'   : [        20e-9,         50e-9,        330e-9,         50e-9,                455e-9, 300e-9,      305e-9,            dC,        dC] # [m]
        }

df = pd.DataFrame(ledStack)

df.doping = 1e6*df.doping
df.mobility = 1e-4*df.mobility

#  df['crossSect'] = np.where(df.lateral == 0,  area, df.thickness * 2 * np.pi * radius)
df['resistance'] = df.apply (lambda x: layerR(x.mobility, x.doping, x.thickness, x.crossSect)[0], axis =1)
df['conductivity'] = df.apply (lambda x: layerR(x.mobility, x.doping, x.thickness, x.crossSect)[1], axis =1)
df['resistivity'] = df.apply (lambda x: layerR(x.mobility, x.doping, x.thickness, x.crossSect)[2], axis =1)
#  df['crossSect'] = area if not df.transversal else thickness * 2 * np.pi * radius
#  df['length'] = dC if df.transversal else df.thickness

print('Layers stack:\n', df, '\n')
#  print('Summary:\n', df.describe(), '\n')
#  print('Quantities Sum:\n', df.sum(), '\n')


# Plots
# -----
res = df['resistance'].sum()
fig = plt.figure()
plt.plot(dC, res, '-o')
plt.xlabel('$d_C (m)$')
plt.ylabel('$R_S (\Omega)$')
plt.show()
#  '''


#  '''
# TODO: parametrize this stack, to see the effect of thickness, doping, mobility, etc
# Define layers and create a dataframe from them
ddsStack = [
         {'name': 'LED Top Contact'        , 'material': 'GaAs'  , 'dopingType': 'p', 'mobility':  400, 'doping': 2.0e19, 'thickness':   20},
         {'name': 'LED p-barrier 1'        , 'material': 'AlGaAs', 'dopingType': 'p', 'mobility':  594, 'doping': 2.0e19, 'thickness':   20},
         {'name': 'LED p-barrier 2'        , 'material': 'AlGaAs', 'dopingType': 'p', 'mobility':  594, 'doping': 4.0e17, 'thickness':  330},
         {'name': 'LED p-barrier 3'        , 'material': 'GaInP' , 'dopingType': 'p', 'mobility':   35, 'doping': 2.0e18, 'thickness':   50},
         {'name': 'LED e-block/p-barrier 4', 'material': 'GaInP' , 'dopingType': 'p', 'mobility':   35, 'doping': 4.0e17, 'thickness':  455},
         {'name': 'LED AR'                 , 'material': 'GaAs'  , 'dopingType': 'i', 'mobility':  500, 'doping': 2.1e16, 'thickness':  300},
         {'name': 'LED n-barrier'          , 'material': 'GaInP' , 'dopingType': 'n', 'mobility': 1000, 'doping': 2.0e17, 'thickness':  305},
         {'name': 'LED/PD Middle Contact'  , 'material': 'GaAs'  , 'dopingType': 'n', 'mobility': 8500, 'doping': 8.0e18, 'thickness':   20},
         {'name': 'LED/PD h-block/CSL'     , 'material': 'GaInP' , 'dopingType': 'n', 'mobility':  500, 'doping': 8.0e18, 'thickness':  810},

         {'name': 'PD AR'                  , 'material': 'GaAs'  , 'dopingType': 'i', 'mobility':  400, 'doping': 2.1e16, 'thickness': 4070},
         {'name': 'PD e-block'             , 'material': 'GaInP' , 'dopingType': 'p', 'mobility':   35, 'doping': 2.0e18, 'thickness':  290},
         {'name': 'PD band offset comp'    , 'material': 'AlGaAs', 'dopingType': 'p', 'mobility':  594, 'doping': 4.0e17, 'thickness':  190},
         #  {'name': 'PD etch stop'       , 'material': 'AlAs'  , 'dopingType': 'p', 'mobility':  , 'doping': 1.0e19, 'thickness':   20}, #TODO: check mobility
         {'name': 'PD bottom contact'      , 'material': 'GaAs'  , 'dopingType': 'p', 'mobility':  400, 'doping': 2.0e19, 'thickness':   20},
        ]

# Create dataframe
df = pd.DataFrame(ddsStack)

# Set SI units
df.doping = 1e6*df.doping
df.mobility = 1e-4*df.mobility
df.thickness = 1e-9*df.thickness

# Reorder columns
df = df[['name', 'material', 'dopingType', 'mobility', 'doping', 'thickness']]
#  print(df)

################
# Define layers
#LED
ledTC  = layer(*df[df.name.str.contains('Top Contact')].values.T)
ledPB1 = layer(*df[df.name.str.contains('p-barrier 1')].values.T)
ledPB2 = layer(*df[df.name.str.contains('p-barrier 2')].values.T)
ledPB3 = layer(*df[df.name.str.contains('p-barrier 3')].values.T)
ledPB4 = layer(*df[df.name.str.contains('p-barrier 4')].values.T)
ledAR  = layer(*df[df.name.str.contains('LED AR')].values.T)
ledAR.isAR = 1
#  ledAR.thickness = 1e-9*np.linspace(1,10,10)
ledNB  = layer(*df[df.name.str.contains('n-barrier')].values.T)
ledMC  = layer(*df[df.name.str.contains('Middle Contact')].values.T)
ledMC.isLateral = 1
ledMC.isSpreading = 1
ledCSL = layer(*df[df.name.str.contains('CSL')].values.T)
ledCSL.isSpreading = 1
ledCSL.isLateral = 1
ledStack = [ledTC, ledPB1, ledPB2, ledPB3, ledPB4, ledAR, ledNB, ledMC, ledCSL]

radii = 1e-6 * np.linspace(250,3000, 5)
dT = 1e-6 * np.linspace(5,100, 10)
dM = 1e-6 * np.linspace(5,200, 20)

radia, dTs = np.meshgrid(radii, dT)
rsf=rSurf(ledStack, radia, dTs)
print(rsf.shape)
data = np.array([radia, dTs, rsf]).reshape(3, -1).T

plt.figure()
plt.plot(dTs, rsf, 'o-', label=radia[0,:][:])
plt.xlabel('$d_T$')
plt.ylabel('$r_{Surf}$')
plt.legend(radia[0,:],title='radius',loc='best')
#  plt.show()

dTs, radia, dMs = np.meshgrid(dT, radii, dM)
rsp=rSpread(ledStack, radia, dTs, dMs)
print(rsp.shape)
#  rsf=rSurf(ledStack, radia, dTs)
#  data = np.array ([radia, dTs, dMs, rsp, rsf]).reshape(5, -1).T
#  data = pd.DataFrame(data, columns = ['radius', 'dT', 'dM', 'rsp', 'rsf'])
#  print(data)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.view_init(45,60)
for i,rad in enumerate(radia[:,:,0][:]):
    ax.plot_surface(dTs[i,:,:], dMs[i,:,:], rsp[i,:,:], label=rad[0])
#  plt.legend(radii)
plt.show()

#PD
pdTC  = layer(*df[df.name.str.contains('Middle Contact')].values.T)
pdTC.isLateral = 0
pdCSL = layer(*df[df.name.str.contains('CSL')].values.T)
pdCSL.isLateral = 0
pdAR  = layer(*df[df.name.str.contains('PD AR')].values.T)
pdEB  = layer(*df[df.name.str.contains('PD e-block')].values.T)
pdEB.isLateral = 0
pdBO  = layer(*df[df.name.str.contains('PD band')].values.T)
pdBO.isLateral = 0
pdBC  = layer(*df[df.name.str.contains('PD bottom contact')].values.T)
pdBC.isLateral = 1
pdStack = [pdTC, pdCSL, pdAR, pdEB, pdBO, pdBC]

# Contribution to resistance
#Define parameters
radius = 200e-6
dT = 100e-6
dM = 50e-6
LED = stack(ledStack, radius, dT, dM)
PD = stack(pdStack, radius, dT, dM)


# Influence of radius, dT and dM
#Define parameters
radius = 1e-6 * np.logspace(2, 3, 4)
dT = 1e-6 * np.linspace(5, 100, 30)
dM = 2e-6 * np.logspace(0,   2, 20)
#Convert arrays to 3D
radius = radius[:,None,None]
dT = dT[None,:,None]
dM = dM[None,None,:]
LED = stack(ledStack, radius, dT, dM)
PD = stack(pdStack, radius, dT, dM)

'''
print ('radius |  dT  |  dM  | $R_{LED}= $ | $R_{PD}=$')
for radius in np.linspace(200e-6, 4000e-6, 20):
    for dT in np.linspace(5e-6, 100e-6, 20):
        for dM in np.linspace(5e-6, 100e-6, 20):
            LED = stack([ledTC, ledPB1, ledPB2, ledPB3, ledPB4, ledAR, ledNB, ledMC, ledCSL], radius, dT, dM)
            PD = stack([pdTC, pdCSL, pdAR, pdEB, pdBO, pdBC], radius, dT, dM)
            #  print('%6.0e | %3.0e | %3.0e | %.3e' %(radius, dT, dM, LED.resistance()))
            #  print('%6.0e | %3.0e | %3.0e | %.9e | %.4e' %(radius, dT, dM, LED.resistance(), PD.resistance()))
'''

colors = [ plt.cm.jet(x) for x in np.linspace(0, 1, len(radius)) ]

fig, ax = plt.subplots(2, 2)
for i,rad in enumerate(radius):
    for j,dm in enumerate(dM[0,0,:]):
        ax[0,0].loglog(dT[0,:,0], LED.resistance()[i,:,j], 'o-',
                color = colors[i],
                #label=r'radius = %.1e, dT = %.1e' %(rad[0,0], dt)
                )
        ax[1,0].loglog(dT[0,:,0], PD.resistance()[i,:,j], 'x-',
                color = colors[i],
                )
ax[0,0].legend(loc='best')
ax[0,0].set_title('LED')
ax[0,0].set_xlabel('$d_{T}$ (m)')
ax[0,0].set_ylabel('$R_S (\Omega)$')
ax[1,0].set_title('PD')
ax[1,0].set_xlabel('$d_{T}$ (m)')
ax[1,0].set_ylabel('$R_S (\Omega)$')
for i,rad in enumerate(radius):
    for j,dt in enumerate(dT[0,:,0]):
        ax[0,1].loglog(dM[0,0,:], LED.resistance()[i,j,:],'o-',
                color = colors[i],
                #label=r'radius = %.1e, dT = %.1e' %(rad[0,0], dt)
                )
        ax[1,1].loglog(dM[0,0,:], PD.resistance()[i,j,:],'x-',
                color = colors[i],
                #label=r'radius = %.1e, dT = %.1e' %(rad[0,0], dt)
                )
ax[0,1].set_title('LED')
ax[0,1].set_xlabel('$d_{M}$ (m)')
ax[0,1].set_ylabel('$R_S (\Omega)$')
ax[1,1].set_title('PD')
ax[1,1].set_xlabel('$d_{M}$ (m)')
ax[1,1].set_ylabel('$R_S (\Omega)$')
##########################################

fig = plt.figure()
ax = fig.gca(projection='3d')
X = dT[0,:,0]
Y = dM[0,0,:]
X, Y = np.meshgrid(X, Y)
Z = LED.resistance()[:,:,:]
for i, rad in enumerate(radius):
    #  surf = ax.scatter(X, Y, LED.rSurf()[i,:,:], 'o', c=colors[i], s=50)
    surf = ax.plot_surface(X, Y, Z[i,:,:], color=colors[i], rstride=100, cstride=100, alpha=0.2, shade=False)
#  ax.set_xscale('log')
#  ax.set_yscale('log')
ax.set_zscale('log')
ax.set_zlim([0,6e1])
ax.set_xlabel('$d_{T}$ ($\mu$m)')
ax.set_ylabel('$d_{M}$ ($\mu$m)')
ax.set_zlabel('$R_S (\Omega)$')

fig = plt.figure()
ax = fig.gca(projection='3d')
X = dT[0,:,0]
Y = dM[0,0,:]
X, Y = np.meshgrid(X, Y)
Z = LED.rSurf()[:,:,:]
for i, rad in enumerate(radius):
    surf = ax.plot_surface(X, Y, Z[i,:,:], color=colors[i], rstride=100, cstride=100, alpha=0.2, shade=False)
#  ax.set_zscale('log')
ax.set_xlabel('$d_{T}$ ($\mu$m)')
ax.set_ylabel('$d_{M}$ ($\mu$m)')
ax.set_zlabel('$R_{Surf} (\Omega)$')

fig = plt.figure()
ax = fig.gca(projection='3d')
X = dT[0,:,0]
Y = dM[0,0,:]
X, Y = np.meshgrid(X, Y)
Z = LED.rSpread()[:,:,:]
for i, rad in enumerate(radius):
    surf = ax.plot_surface(X, Y, Z[i,:,:], color=colors[i], rstride=100, cstride=100, alpha=0.2, shade=False)
#  ax.set_zscale('log')
ax.set_xlabel('$d_{T}$ ($\mu$m)')
ax.set_ylabel('$d_{M}$ ($\mu$m)')
ax.set_zlabel('$R_{Spread} (\Omega)$')
plt.show()

#  '''
