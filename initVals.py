import sys
# access to all other packages in the upper directory
sys.path.append('../')
from physics.constants import ureg, Q_


# Temperature
T = Q_(298, 'K')

# Active region thickness
ARthickness = Q_(300, 'nm')

# SRH-like, non radiative recombination rate
#  A = Q_(8e05, 's**-1')
A = Q_(1e06, 's**-1')

# Radiative recombination rate
#  B = Q_(2.5e-12, 'cm**3 / s')
B = Q_(1.0e-16, 'm**3 / s')

# Auger recombination rate
#  C = Q_(2e-30, 'cm**6 / s')
C = Q_(1e-42, 'm**6 / s')

# SRH-like, non radiative recombination rate in the surface
Asurf = Q_(1e4, 's**-1')

# Radiative recombination rate in the surface
Bsurf = Q_(0, 'cm**3 / s')

# Auger recombination rate in the surface
Csurf = Q_(0, 'cm**6 / s')

# Surface diode limit current
i0SF = Q_(1e-14, 'A') #/ cm**2

# Optical losses
chi = 0.7

# Number of AR distributed diodes
ARNdio = 50

# Shunt resistance
rSh = Q_(100, 'Mohm') #* cm**2

# Surface resistance
rSurf = Q_(100, 'ohm')

# Spreading layer resistance
rSpread = Q_(1.0, 'ohm')

#  # LED diode limit current
#  i0L = Q_(1e-8, 'A') #/ cm**2
#
#  # PD diode limit current
#  i0P = Q_(5e-12, 'A') #/ cm**2
#
