##############################
# Import packages and modules
##############################
import time
stime = time.time()
print('Elapsed time {0:.3f}: Initialise'.format(time.time() - stime))
import warnings
import pandas as pd
import numpy as np
from scipy.interpolate import interp1d
import matplotlib as mpl
import matplotlib.pyplot as plt
from pprint import pprint
from texttable import Texttable

import functions as funs
import initVals as ini
from stack import ledStack

import sys
# access to all other packages in the upper directory
sys.path.append('./')
sys.path.append('../')
from physics.constants import ureg, Q_
import physics.constants as pc

print('Elapsed time {0:.3f}: Loaded modules'.format(time.time() - stime))

###########################
# Modules/packages options
###########################
warnings.filterwarnings("ignore")

np.set_printoptions(threshold=np.inf)

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 0)

plt.rcParams.update(plt.rcParamsDefault)

#  plt.style.use('bmh')
#  plt.style.use('tableau-colorblind10')
#  plt.style.use('fivethirtyeight')
#  plt.style.use('seaborn-darkgrid')
#  plt.style.use('dark_background')
plt.style.use('ggplot')

plt.rcParams.update({
    'text.usetex'       : True,
    'axes.edgecolor'    : 'dimgrey',
    'axes.grid'         : False,
    'axes.facecolor'    : 'grey',
    'font.size'         : 5.0,
    'lines.linewidth'   : 1.0,
    'lines.markersize'  : 1.0,
    'legend.shadow'     : False,
    'legend.fancybox'   : False,
    'legend.fontsize'   : 4.5,
    #  'font.family': 'serif',
    #  'xtick.labelsize' : 17,
    #  'ytick.labelsize' : 17,
    'savefig.format'     : 'jpg',
    })


#####################
# Plotting functions
#####################
def pltRadiusEffect(
        uAR, diams, dTs, dMs,
        #  colormap=plt.cm.Spectral,
        colormap=plt.cm.Greens,
        title='Effect of Diameter',
        savePlot=False, showPlot=True,
        saveDir='./output/',
    ):

    fig, ax = plt.subplots(3,7)
    fig.suptitle(title)
    ax0, ax1, ax2 = ax
    colors = [ colormap(x) for x in np.linspace(0, 1, len(rads)) ]

    for ii, (dT, dM) in enumerate(zip(dTs, dMs)):

        cqegs = np.empty(0)
        cqegsIntrp = np.empty(0)

        for jj, diam in enumerate(diams):
            dev = funs.LED (**{
                        'chi'         : ini.chi,
                        'radius'      : diam/2,
                        'dT'          : dT,
                        'dM'          : dM,
                        'uAR'         : uAR,
                        'stack'       : ledStack,
                        'ARthickness' : ini.ARthickness,
                        'A'           : ini.A,
                        'B'           : ini.B,
                        'C'           : ini.C,
                        'Asurf'       : ini.Asurf,
                        'Bsurf'       : ini.Bsurf,
                        'Csurf'       : ini.Csurf,
                        'T'           : ini.T,
                        'rSh'         : ini.rSh,
                        'i0SF'        : ini.i0SF,
                        'ARNdio'      : ini.ARNdio,
                    })

            ax0[ii].semilogy (
                    dev.u.to_base_units().magnitude,
                    abs(dev.i.to_base_units().magnitude),
                    '-', c=colors[jj],
                    label = f'${diam: .0f~L}'
                )

            ax1[ii].plot(
                    dev.u.to_base_units().magnitude,
                    dev.cqe.to_base_units().magnitude,
                    '-', c=colors[jj],
                    label = f'${diam: .0f~L}$'
                )

            cqegs = np.append (cqegs, dev.cqeG)
            cqegsIntrp = np.append (cqegsIntrp, dev.cqegIntrp)

        # Plot cqe at Eg bias, from the real curve:
        ax2[ii].plot (diams, cqegs, 'x', label='as is')
        # its spline:
        f2 = interp1d(diams, cqegs, kind='quadratic')
        diams2 = np.linspace(min(diams), max(diams), 50)
        ax2[ii].plot (diams2, f2(diams2), '-')

        # and from the interpolated cqeg curves:
        #  print(diams, cqegsIntrp)
        #  ax2[ii].plot (diams, cqegsIntrp, 'o--', label='interpolated')


        ################################
        # Legend
        ax1[ii].legend(loc='lower right', title='$D$')

        # Text annotation with initial values of different parameters
        textstr = '\n'.join((
            f'$A       = {ini.A: 0.1e~L}$',
            f'$B       = {ini.B: 0.1e~L}$',
            f'$A_{{SF}} = {ini.Asurf: 0.1e~L}$',
            f'$R_{{SH}} = {ini.rSh: .1f~L}$',
            f'$d(AR) = {ini.ARthickness: .1f~L}$',
            ))
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        ax0[ii].text(
                    0.98, 0.02, textstr,
                    transform=ax0[ii].transAxes,
                    fontsize=3,
                    verticalalignment='bottom', horizontalalignment='right',
                    bbox=props
                )

        # Subplots titles
        ax0[ii].set_title(f'$d_{{T}} = {dT: .0f~L}$\n$d_{{M}} = {dM: .0f~L}$')

        # Y axis only visible in the first column subplots
        if ii > 0:
            ax0[ii].yaxis.set_visible(False)
            ax1[ii].yaxis.set_visible(False)
            ax2[ii].yaxis.set_visible(False)

        # Axis properties:
        ax0[ii].set_xlabel(f'$U ({dev.u.units:~L})$')
        ax0[ii].set_ylabel(f'$I ({dev.i.to_base_units().units:~L})$')
        #  ax0[ii].set_ylabel('$I$')
        ax0[ii].set_xlim([-0.3,3.0])
        ax0[ii].set_ylim([1e-9,3e1])

        ax1[ii].set_xlabel(f'$U ({dev.u.units:~L})$')
        ax1[ii].set_ylabel('$CQE$')
        ax1[ii].set_xlim([0.9,4])
        ax1[ii].set_ylim([0.0,0.8])

        ax2[ii].set_xlabel(f'$D ({diams.units:~L})$')
        ax2[ii].set_ylabel('$CQE_{GAP}$')

    if savePlot:
        plt.savefig(saveDir + 'effect_Diameter', dpi=200)

    if showPlot:
        fig.set_dpi(250)

    return fig

def pltDistancesAndR (
        deviceList, rads, dTs, dMs,
        colormap=plt.cm.Greens,
        title='Effect of Radius',
        savePlot=False, saveDir='./output/', showPlot=True
    ):
    '''
    Effect of the distance dM (between top mesa and middle contact
    '''

    colors = [ colormap(x) for x in np.linspace(0, 1, len(dTs)) ]

    for rad in rads:

        fig, ax = plt.subplots(1, 2)
        ax0, ax1 = ax
        #  fig.tight_layout()
        fig.suptitle(title + f' $(Radius = {rad:.0f~L})$')

        for ii, (dT, dM) in enumerate(zip(dTs, dMs)):

            for dev in deviceList:

                if dev.dT == dT and dev.dM == dM and dev.radius==rad:

                    ax0.semilogy(
                            dev.u.to_base_units().magnitude,
                            abs(dev.i.to_base_units().magnitude),
                            '-', c=colors[ii]
                        )

                    ax1.plot(
                            dev.u.magnitude,
                            dev.cqe.magnitude,
                            '-', c=colors[ii],
                            label=(f'$d_T = {dT:2.0f~L}, d_M = {dM:2.0f~L}, R^T_{{SP}} = {dev.rSPT.to(ureg.ohm):3.2f~L}, R_{{SF}} = {dev.rSF.to(ureg.ohm):3.0f~L}$')
                        )

            ax0.set_xlabel('$U_L$'), ax0.set_ylabel('$I$')
            ax0.set_xlim([-0.0,3]), ax0.set_ylim([1e-5,1e2])

            textstr = '\n'.join((
                f'$A={ini.A:0.1e~L}$',
                f'$B={ini.B:0.1e~L}$',
                f'$C={ini.C:0.1e~L}$',
                f'$A_{{SF}}={ini.Asurf:0.1e~L}$',
                ))
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            ax0.text(
                    0.60, 0.20, textstr,
                    transform=ax0.transAxes,
                    fontsize=3,
                    verticalalignment='top', bbox=props)

            ax1.set_xlabel('$U_L$'), ax1.set_ylabel('$CQE$')
            ax1.set_xlim([1.0,3.0]), ax1.set_ylim([0.50,1.0])
            ax1.legend(loc='best', ncol=1)

        if savePlot:

            plt.savefig(saveDir + f'effect_DistanceAndR-{rad.magnitude:.0f}',
                dpi=200)

        if showPlot:

            fig.set_dpi(250)

    return fig

def plt_dT_effect (
        deviceList, rads, dTs, dMs,
        colormap=plt.cm.Greens,
        savePlot=False, saveDir='./output/', showPlot=True
    ):
    '''
    Effect of the distance dT (between top mesa and middle contact
    '''
    colors = [ colormap(x) for x in np.linspace(0, 1, len(dTs)) ]
    for radius in rads:
        fig, ax = plt.subplots(2, len(dMs))
        #  fig.tight_layout()
        title=(f'Effect of $d_T (Radius = {radius:.0f~L})$')
        fig.suptitle(title)
        for ii, dM in enumerate(dMs):
            ax0 = ax[0,ii]
            ax1 = ax[1,ii]
            for jj, dT in enumerate(dTs):
                for dev in deviceList:
                    if dev.radius == radius and dev.dT == dT and dev.dM==dM:
                        ax0.semilogy(
                                dev.u.to_base_units().magnitude,
                                abs(dev.i.to_base_units().magnitude),
                                c=colors[jj], linestyle='-'
                            )
                        ax1.plot(
                                dev.u.magnitude,
                                dev.cqe.magnitude,
                                c=colors[jj], linestyle='-',
                                label=(f'$d_T = {dev.dT:2.0f~L}$')
                            )

            # Y axis only visible in the first column subplots
            if ii > 0:
                ax0.yaxis.set_visible(False)
                ax1.yaxis.set_visible(False)

            ax0.set_title(f'$d_{{M}}= {dM:0.0f~L}$')
            ax0.set_xlabel('$U$'), ax0.set_ylabel('$I$')
            ax0.set_xlim([-0.0,4.0]), ax0.set_ylim([1e-5,1e2])

            textstr = '\n'.join((
                f'$A={ini.A:0.1e~L}$',
                f'$B={ini.B:0.1e~L}$',
                f'$C={ini.C:0.1e~L}$',
                f'$A_{{SF}}={ini.Asurf:0.1e~L}$',
                ))
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            ax0.text(
                    0.99, 0.01, textstr,
                    transform=ax0.transAxes,
                    horizontalalignment='right',
                    verticalalignment='bottom',
                    fontsize=3,
                    bbox=props
                )

            ax1.set_xlabel('$U$'), ax1.set_ylabel('$CQE$')
            ax1.set_xlim([1.0,4.0]), ax1.set_ylim([0.6,1])
            ax1.legend(loc='best', ncol=1)

        if savePlot:
            plt.savefig(saveDir + f'effect_dT-{radius.magnitude:.0f}',
                    dpi=200)

        if showPlot:
            fig.set_dpi(250)

    return fig

def plt_dM_effect (
        deviceList, rads, dTs, dMs,
        colormap=plt.cm.Greens,
        savePlot=False, saveDir='./output/', showPlot=True
    ):
    '''
    Effect of the distance dM (between top mesa and middle contact
    '''
    colors = [ colormap(x) for x in np.linspace(0, 1, len(dMs)) ]
    for radius in rads:
        fig, ax = plt.subplots(2, len(dTs))
        #  fig.tight_layout()
        title=(f'Effect of $d_M (Radius = {radius:.0f~L})$')
        fig.suptitle(title)
        for ii, dT in enumerate(dTs):
            ax0 = ax[0,ii]
            ax1 = ax[1,ii]
            for jj, dM in enumerate(dMs):
                for dev in deviceList:
                    if dev.radius == radius and dev.dT == dT and dev.dM==dM:
                        ax0.semilogy(
                                dev.u.to_base_units().magnitude,
                                abs(dev.i.to_base_units().magnitude),
                                c=colors[jj], linestyle='-'
                            )
                        ax1.plot(
                                dev.u.to_base_units().magnitude,
                                dev.cqe.to_base_units().magnitude,
                                c=colors[jj], linestyle='-',
                                label=(f'$d_M = {dev.dM:.0f~L}$')
                            )

            # Y axis only visible in the first column subplots
            if ii > 0:
                ax0.yaxis.set_visible(False)
                ax1.yaxis.set_visible(False)

            ax0.set_title(f'$d_{{T}}= {dT:0.0f~L}$')
            ax0.set_xlabel('$U$'), ax0.set_ylabel('$I$')
            ax0.set_xlim([-0.0,4.0]), ax0.set_ylim([1e-5,1e2])

            textstr = '\n'.join((
                f'$A={ini.A:0.1e~L}$',
                f'$B={ini.B:0.1e~L}$',
                f'$C={ini.C:0.1e~L}$',
                f'$A_{{SF}}={ini.Asurf:0.1e~L}$',
                ))
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            ax0.text(
                    0.99, 0.01, textstr,
                    transform=ax0.transAxes,
                    horizontalalignment='right',
                    verticalalignment='bottom',
                    fontsize=3,
                    bbox=props
                )

            ax1.set_xlabel('$U$'), ax1.set_ylabel('$CQE$')
            ax1.set_xlim([1.0,4.0]), ax1.set_ylim([0.6,1])
            ax1.legend(loc='best', ncol=1)

        if savePlot:
            plt.savefig(saveDir + f'effect_dM-{radius.magnitude:.0f}',
                    dpi=200)

        if showPlot:
            fig.set_dpi(250)

    return fig

def pltDistances (
        uAR,
        diams, dTs, dMs,
        title='Effect of ...',
        colormap=plt.cm.Greens,
        savePlot=False, saveDir='./output/', showPlot=True,
        showTable=False, showLayersResistance=False,
    ):

    '''
    Effect of the distance dM (between top mesa and middle contact
    '''

    colors = [ colormap(x) for x in np.linspace(0, 1, len(dTs)) ]

    fig, ax = plt.subplots(2, len(diams))
    fig.suptitle(title)

    if showTable:
        data = [[
            'Diameter (micron)',
            'dT (micron)',
            'dM (micron)',
            'rSF (ohm)',
            'rSP (ohm)'
            ]]

    for jj, dia in enumerate(diams):

        ax0, ax1 = ax[0,jj], ax[1,jj]

        for ii, (dT, dM) in enumerate(zip(dTs, dMs)):

            dev = funs.LED (**{
                        'chi'         : ini.chi,
                        'radius'      : dia/2,
                        'dT'          : dT,
                        'dM'          : dM,
                        'uAR'         : uAR,
                        'stack'       : ledStack,
                        'ARthickness' : ini.ARthickness,
                        'A'           : ini.A,
                        'B'           : ini.B,
                        'C'           : ini.C,
                        'Asurf'       : ini.Asurf,
                        'Bsurf'       : ini.Bsurf,
                        'Csurf'       : ini.Csurf,
                        'T'           : ini.T,
                        'rSh'         : ini.rSh,
                        'i0SF'        : ini.i0SF,
                        'ARNdio'      : ini.ARNdio,
                    })

            if showLayersResistance:
                dev.summary(saveTable=False)

            lbl=(f'${dT:2.0f~L}, \
                    {dM:2.0f~L}, \
                    {dev.rSP.to(ureg.ohm):3.2f~L}, \
                    {dev.rSF.to(ureg.ohm):3.0f~L}$')

            if showTable:
                data.append ([ 
                        dev.diameter.magnitude,
                        dev.dT.magnitude,
                        dev.dM.magnitude,
                        dev.rSF.magnitude,
                        dev.rSP.magnitude,
                    ])

            # LED IV
            ax0.semilogy(
                    dev.u,
                    abs(dev.i),
                    '-', c=colors[ii], label=lbl,
                )

            # EQE
            ax1.plot(
                    dev.u,
                    dev.eqe,
                    '-', c=colors[ii], label=lbl,
                )

            # AR diode current
            ax0.semilogy(
                    dev.u,
                    abs(dev.iAR),
                    ':', c=colors[ii],
                )

            # Injection efficiency
            ax1.plot(
                    dev.u,
                    dev.injEff,
                    ':', c=colors[ii],
                )

            # Radiative component
            ax0.semilogy(
                    dev.u,
                    abs(dev.iARrad),
                    '--', c=colors[ii],
                )

            # IQE
            ax1.plot(
                    dev.u,
                    dev.iqe,
                    '--', c=colors[ii],
                )

            # Non-radiative component
            #  ax0.semilogy(
            #          dev.u.to_base_units().magnitude,
            #          abs(dev.iARsrh.to_base_units().magnitude),
            #          ':', c=colors[ii],
            #      )


            ax0.set_title(f'$D = {dia:.0f~L}$')
            ax0.set_xlabel('$U_L$'), ax0.set_ylabel('$I$')
            ax0.set_xlim([-0.0,4.5]), ax0.set_ylim([1e-11,1e1])
            ax0.legend(loc='lower right', ncol=1,
                    title=f'$d_T, d_M, R_{{SP}}, R_{{SF}}$'
                    )

            textstr = '\n'.join((
                f'$A={ini.A:0.1e~L}$',
                f'$B={ini.B:0.1e~L}$',
                f'$C={ini.C:0.1e~L}$',
                f'$A_{{SF}}={ini.Asurf:0.1e~L}$',
                f'$R_{{SH}}={ini.rSh:0.1e~L}$',
                f'$\chi={ini.chi:0.1f}$',
                f'$N_{{AR,dio}} = {ini.ARNdio:.0f}$',
                ))
            props = dict(boxstyle='round', facecolor='wheat', alpha=1.0)
            ax0.text(0.02, 0.98, textstr,
                    transform=ax0.transAxes,
                    fontsize=3,
                    horizontalalignment='left',verticalalignment='top',
                    bbox=props)

            ax1.set_xlabel('$U_L$'), ax1.set_ylabel('$QE$')
            ax1.set_xlim([0.0,4.5]), ax1.set_ylim([0.00,1.0])

            linestyles = ['-', ':', '--']
            lines = [mpl.lines.Line2D([0], [0], linestyle=l) for l in linestyles]
            labels = [f'$EQE$', f'$IQE$', f'$\eta_{{inj}}$']
            ax1.legend(lines, labels)

        if savePlot:

            filename = title.\
                replace('|','').\
                replace('(','').\
                replace(')','').\
                replace('_','').\
                replace(' ','_').\
                replace('$','')

            output = '/'.join([saveDir,filename])
            fig.savefig(output, dpi=200)

        if showPlot:

            fig.set_dpi(250)

    if showTable:
        t = Texttable (max_width=0)
        t.add_rows(data)
        t.set_cols_align(5*['c'])
        t.set_cols_valign(5*['t'])
        t.set_cols_dtype(5*['a'])
        # t.set_chars ([horizontal, vertical, corner, header])
        # default is set to: ['-', '|', '+', '=']
        t.set_chars (['','|','','-'])
        t.set_deco (Texttable.VLINES | Texttable.BORDER)
        print (t.draw())

    return fig

###########################################################################
print('Elapsed time {0:.3f}: Load values'.format(time.time() - stime))
###########################################################################

# Surface diode voltage:
uAR = np.linspace (-1.0, 4, 5000) * ureg.volt
#  print(uAR._check('[electric potential]'))

# Radii:
#  rads = 1e-6 * np.linspace(100, 2000, 50)
#  rads = 1e-6 * np.arange(100, 2000, 25)
rads = np.array([125, 250, 500, 1000]) * ureg.microns

# Contact distances:
#  dMs = [5, 10, 20, 40, [50, 25], [25, 75], [5, 95]] * ureg.microns
#  dTs = np.linspace(5, 200, 50) * 1e-6
#  dMs = np.linspace(5, 200, 50) * 1e-6
dTs = np.array([5, 10, 20, 40, 50, 75, 95]) * ureg.microns
dMs = np.array([5, 10, 20, 40, 50, 75, 95]) * ureg.microns

###########################################################################
print('Elapsed time {0:.3f}: Create devices'.format(time.time() - stime))
###########################################################################

# Create a device list:
#  ledList = funs.createDevList2 (
#                      funs.LED, {
#                          'radius'     : rads,
#                          'dT'         : dTs,
#                          'dM'         : dMs
#                      }, **{
#                          'uAR'        : uAR,
#                          'stack'      : ledStack,
#                          'ARthickness': ini.ARthickness,
#                          'A'          : ini.A,
#                          'B'          : ini.B,
#                          'C'          : ini.C,
#                          'i0SF'       : ini.i0SF,
#                          'Asurf'      : ini.Asurf,
#                          'Bsurf'      : ini.Bsurf,
#                          'Csurf'      : ini.Csurf,
#                          'T'          : ini.T,
#                          'rSh'        : ini.rSh,
#                          'chi'        : ini.chi,
#                          'ARNdio'     : ini.ARNdio,
#                      },
#                      #  trackTime=True
#                  )

##############################################################################
print('Elapsed time {0:.3f}: Plots'.format(time.time() - stime))
##############################################################################
cm = plt.cm.Spectral
cm = plt.cm.Reds

diams = 2 * rads
pltRadiusEffect (uAR, diams, dTs, dMs, colormap=cm, savePlot=True)

#  pltDistancesAndR (ledList, rads, dTs, dMs, colormap = cm,  savePlot=True)
#  plt_dT_effect (ledList, rads, dTs, dMs, colormap = cm, savePlot=True)
#  plt_dM_effect (ledList, rads, dTs, dMs, colormap = cm, savePlot=True)

diams = np.array([250, 500, 1000]) * ureg.microns
pltDistances (
        uAR,
        diams, dTs, dMs,
        colormap = cm,
        savePlot = False,
        title = f'Effect of $d_T$',
        showTable=True, showLayersResistance=True,
    )
# TODO: find a way to export this to eps without the error postscript backend does not support transparency

##############################################################################
print('Elapsed time {0:.3f}: End'.format(time.time() - stime))
##############################################################################
plt.show()
