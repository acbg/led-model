import sys
sys.path.append('../') # access to all other packages in the upper directory
import functions as funs
from physics.constants import ureg, Q_, q, k

nGaAs = funs.Material (
        name = 'n-GaAS',
        T0 = Q_(204, 'K'),
        Eg0 = Q_(1.519,'eV'),
        EgA = Q_(5.405e-4, 'eV/K'),
        El0 = Q_(1.815, 'eV'),
        ElA = Q_(6.05e-4, 'eV/K'),
        Ex0 = Q_(1.981, 'eV'),
        ExA = Q_(4.6e-4, 'eV/K'),
        Tv = Q_(1.83e15, 'K**-1.5 cm**-3'),
        Tc0  = Q_(8.63e13, 'K**-1.5'),
        Tc1 = Q_(1, 'cm**-3'),
        Tc2 = Q_(1.931e-4, 'K**-1 cm**-3'),
        Tc3 = Q_(4.190e-8, 'K**-2 cm**-3'),
        Tc4 = Q_(21,'cm**-3'),
        Tc5 = Q_(44,'cm**-3'),
        Chw = 1.8,
        muMax = Q_(9400, 'cm**2 / V / s'),
        muMin = Q_(500, 'cm**2 / V / s'),
        Nref = Q_(6.0e16, 'cm**-3'),
        lamb = 0.394,
        theta1 = 2.1,
        theta2 = 3.0,
        )

pGaAs = funs.Material (
        name = 'p-GaAS',
        T0 = Q_(204, 'K'),
        Eg0 = Q_(1.519,'eV'),
        EgA = Q_(5.405e-4, 'eV/K'),
        El0 = Q_(1.815, 'eV'),
        ElA = Q_(6.05e-4, 'eV/K'),
        Ex0 = Q_(1.981, 'eV'),
        ExA = Q_(4.6e-4, 'eV/K'),
        Tv = Q_(1.83e15, 'K**-1.5 cm**-3'),
        Tc0  = Q_(8.63e13, 'K**-1.5'),
        Tc1 = Q_(1, 'cm**-3'),
        Tc2 = Q_(1.931e-4, 'K**-1 cm**-3'),
        Tc3 = Q_(4.190e-8, 'K**-2 cm**-3'),
        Tc4 = Q_(21,'cm**-3'),
        Tc5 = Q_(44,'cm**-3'),
        Chw = 1.8,
        muMax = Q_(491.5, 'cm**2 / V / s'),
        muMin = Q_(20, 'cm**2 / V / s'),
        Nref = Q_(1.48e17, 'cm**-3'),
        lamb = 0.38,
        theta1 = 2.2,
        theta2 = 3.0,
        )

pAlGaAs = funs.Material ( #Al0.3Ga0.7As
        name = 'p-AlGaAs',
        T0 = Q_(204, 'K'),
        Eg0 = Q_(1.424,'eV') + 0.3*Q_(1.247,'eV'),
        EgA = Q_(5.405e-4, 'eV/K'), #not sure about this...
        El0 = Q_(1.519,'eV') + Q_(1.155, 'eV')*0.3 + Q_(0.37, 'eV')*0.3**2,
        ElA = Q_(5.41e-4, 'eV/K'),
        Ex0 = Q_(1.981, 'eV') + Q_(0.124, 'eV')*0.3 + Q_(0.144, 'eV')*0.3**2,
        ExA = Q_(4.6e-4, 'eV/K'),
        Tv = Q_(4.82e15 * (0.51 + 0.25*0.3)**1.5, 'K**-1.5 cm**-3'),
        Tc0  = Q_(4.82e15 * (0.063+0.083*0.3)**1.5, 'K**-1.5'),
        Tc1 = Q_(1, 'cm**-3'),
        Tc2 = Q_(0, 'K**-1 cm**-3'),
        Tc3 = Q_(0, 'K**-2 cm**-3'),
        Tc4 = Q_(0,'cm**-3'),
        Tc5 = Q_(0,'cm**-3'),
        Chw = 1.8,
        muMax = Q_(240, 'cm**2 / V / s'),
        muMin = Q_(5, 'cm**2 / V / s'),
        Nref = Q_(1.00e17, 'cm**-3'),
        lamb = 0.324,
        theta1 = 1.0,
        theta2 = 1.0,
        )

nInGaP = funs.Material ( #
        name = 'n-InGaP',
        T0  = Q_(0, 'K'),
        Eg0 = Q_(0,'eV'),
        EgA = Q_(0, 'eV/K'),
        El0 = Q_(0,'eV'),
        ElA = Q_(0, 'eV/K'),
        Ex0 = Q_(0, 'eV'),
        ExA = Q_(0, 'eV/K'),
        Tv  = Q_(0, 'K**-1.5 cm**-3'),
        Tc0 = Q_(0, 'K**-1.5'),
        Tc1 = Q_(0, 'cm**-3'),
        Tc2 = Q_(0, 'K**-1 cm**-3'),
        Tc3 = Q_(0, 'K**-2 cm**-3'),
        Tc4 = Q_(0,'cm**-3'),
        Tc5 = Q_(0,'cm**-3'),
        Chw = 0,
        muMax  = Q_(4300, 'cm**2 / V / s'),
        muMin  = Q_(400, 'cm**2 / V / s'),
        Nref   = Q_(2.00e16, 'cm**-3'),
        lamb   = 0.7,
        theta1 = 1.6,
        theta2 = 1.0,
        )

pInGaP = funs.Material ( #
        name = 'p-InGaP',
        T0  = Q_(0, 'K'),
        Eg0 = Q_(0,'eV'),
        EgA = Q_(0, 'eV/K'),
        El0 = Q_(0,'eV'),
        ElA = Q_(0, 'eV/K'),
        Ex0 = Q_(0, 'eV'),
        ExA = Q_(0, 'eV/K'),
        Tv  = Q_(0, 'K**-1.5 cm**-3'),
        Tc0 = Q_(0, 'K**-1.5'),
        Tc1 = Q_(0, 'cm**-3'),
        Tc2 = Q_(0, 'K**-1 cm**-3'),
        Tc3 = Q_(0, 'K**-2 cm**-3'),
        Tc4 = Q_(0,'cm**-3'),
        Tc5 = Q_(0,'cm**-3'),
        Chw = 0,
        muMax  = Q_(150, 'cm**2 / V / s'),
        muMin  = Q_(15, 'cm**2 / V / s'),
        Nref   = Q_(1.50e17, 'cm**-3'),
        lamb   = 0.8,
        theta1 = 2.0,
        theta2 = 1.0,
        )

#  print(pGaAs.Eg(T=300*ureg.K))
#  print(pGaAs.carrDensity(u=1*ureg.V, T=300*ureg.K))
#  print(nGaAs.mobility(T=300*ureg.K))
#  print(pGaAs.mobility(T=300*ureg.K))

# Define LED layers:
ledTC = funs.Layer(
            name='LED Top Contact', material=pGaAs, dopingType='p',
            #  mobility = Q_(400, 'cm**2 / volt / s'),
            doping = Q_(2.0e19, 'cm**-3'),
            thickness = Q_(20, 'nm'),
        )
ledTC.isSurf = True
#  print(ledTC.mobility(300*ureg.K))
#  print(ledTC.rSF(3, 4))

ledPB1 = funs.Layer(
            name='LED p-barrier 1', material=pAlGaAs, dopingType='p',
            #  mobility=594 * ureg.cm**2 / ureg.volt / ureg.s,
            doping=1.0e19 * ureg.cm**-3,
            thickness=50 * ureg.nm,
        )
ledPB1.isSurf = True
#  print(ledPB1.mobility(300*ureg.K))

ledPB2 = funs.Layer(
            name='LED p-barrier 2', material=pAlGaAs, dopingType='p',
            #  mobility=594 * ureg.cm**2 / ureg.volt / ureg.s,
            doping=4.0e17 * ureg.cm**-3,
            thickness=330 * ureg.nm,
        )
ledPB2.isSurf = True

ledPB3 = funs.Layer(
            name='LED p-barrier 3', material=pInGaP, dopingType='p',
            #  mobility=35 * ureg.cm**2 / ureg.volt / ureg.s,
            doping=2.0e18 * ureg.cm**-3,
            thickness=50 * ureg.nm,
        )
ledPB3.isSurf = True

ledPB4 = funs.Layer(
            name='LED e-block/p-barrier 4', material=pInGaP, dopingType='p',
            #  mobility=35 * ureg.cm**2 / ureg.volt / ureg.s,
            doping=4.0e17 * ureg.cm**-3,
            thickness=455 * ureg.nm,
        )
ledPB4.isSurf = True
#  print(ledPB4.mobility(300*ureg.K))

ledAR = funs.Layer(
            name='LED AR', material=nGaAs, dopingType='i',
            #  mobility=500 * ureg.cm**2 / ureg.volt / ureg.s,
            doping=1.0e-11 * ureg.cm**-3,
            thickness=303 * ureg.nm,
        )
ledAR.isAR = 1
#  ledAR.thickness = 1e-9*np.linspace(1,10,10)
#  print(ledAR.mobility(300*ureg.K))

ledNB = funs.Layer(
            name='LED n-barrier', material=nInGaP, dopingType='n',
            #  mobility=1000 * ureg.cm**2 / ureg.volt / ureg.s,
            doping=2.0e17 * ureg.cm**-3,
            thickness=305 * ureg.nm,
        )
ledNB.isSpreading = True
#  print(ledNB.mobility(300*ureg.K))

ledMC = funs.Layer(
            name='LED/PD Middle Contact', material=nGaAs, dopingType='n',
            #  mobility=8500 * ureg.cm**2 / ureg.volt / ureg.s,
            doping=8.0e18 * ureg.cm**-3,
            thickness=20 * ureg.nm,
        )
ledMC.isSpreading = True
#  print(ledMC.mobility(300*ureg.K))

ledCSL = funs.Layer(
            name='LED/PD h-block/CSL', material=nInGaP, dopingType='n',
            #  mobility=500 * ureg.cm**2 / ureg.volt / ureg.s,
            doping=8.0e18 * ureg.cm**-3,
            thickness=810 * ureg.nm,
        )
ledCSL.isSpreading = 1
#  print(ledCSL.mobility(300*ureg.K))

ledStack = [ledTC, ledPB1, ledPB2, ledPB3, ledPB4, ledAR, ledNB, ledMC, ledCSL]

ledStack = funs.Stack (ledStack)



