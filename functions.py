# TODO: gui with sliders to visualize effect of the different parameters on the IV curve

import itertools
from time import time
import numpy as np
from scipy.special import lambertw
from prettytable import PrettyTable
from texttable import Texttable

########################
# Physical constants
########################
import sys
# access to all other packages in the upper directory
sys.path.append('./')
sys.path.append('../')
from physics.constants import ureg, Q_
import physics.constants as pc
import physics.functions as pf

##############
# Resistances
##############
def rSurf (stack, rInt, rExt):
    '''
    surface resistance
    '''

    rSurf = 0

    for layer in stack:
        mobility = layer.mobility
        doping=layer.doping
        thickness = layer.thickness
        rSurf = rSurf + pf.rCirc (mobility, doping, thickness, rInt, rExt)\
                if layer.isSurf == True and not layer.isAR\
                else rSurf

    return rSurf

def rSpread (stack, rInt, rExt):
    '''
    spreading resistance
    '''

    rSpread = 0

    for layer in stack:
        mobility = layer.mobility
        doping=layer.doping
        thickness = layer.thickness
        rSpread = rSpread + pf.rCirc (mobility, doping, thickness, rInt, rExt) \
                if layer.isSpreading \
                else rSpread

    return rSpread

##########
# Classes
############

class Material:

    def __init__(
            self,
            name   = '',
            T0     = Q_(0, 'K'),
            Eg0    = Q_(0,'eV'),
            EgA    = Q_(0, 'eV/K'),
            El0    = Q_(0, 'eV'),
            ElA    = Q_(0, 'eV/K'),
            Ex0    = Q_(0, 'eV'),
            ExA    = Q_(0, 'eV/K'),
            Tv     = Q_(0, 'K**-1.5 cm**-3'),
            Tc0    = Q_(0, 'K**-1.5'),
            Tc1    = Q_(0, 'cm**-3'),
            Tc2    = Q_(0, 'K**-1 cm**-3'),
            Tc3    = Q_(0, 'K**-2 cm**-3'),
            Tc4    = Q_(0,'cm**-3'),
            Tc5    = Q_(0,'cm**-3'),
            Chw    = 0,
            muMax  = Q_(0, 'cm**2 / V / s'),
            muMin  = Q_(0, 'cm**2 / V / s'),
            Nref   = Q_(0, 'cm**-3'),
            lamb   = 0.38,
            theta1 = 2.2,
            theta2 = 3.0,
        ):

        self.name   = name
        self.T0     = T0
        self.Eg0    = Eg0
        self.EgA    = EgA
        self.El0    = El0
        self.ElA    = ElA
        self.Ex0    = Ex0
        self.ExA    = ExA
        self.Tv     = Tv
        self.Tc0    = Tc0
        self.Tc1    = Tc1
        self.Tc2    = Tc2
        self.Tc3    = Tc3
        self.Tc4    = Tc4
        self.Tc5    = Tc5
        self.Chw    = Chw
        self.muMax  = muMax
        self.muMin  = muMin
        self.Nref   = Nref
        self.lamb   = lamb
        self.theta1 = theta1
        self.theta2 = theta2

    def Eg (self, T):
        '''
        from IOFFE, http://www.ioffe.ru/SVA/NSM/Semicond/
        '''

        Eg = self.Eg0 - self.EgA * T**2 / (T + self.T0)

        return Eg

    def El (self, T):
        '''
        from IOFFE, http://www.ioffe.ru/SVA/NSM/Semicond/
        '''

        El =  self.El0 - self.ElA * T**2 / (T + self.T0)

        return El

    def Ex (self, T):
        '''
        from IOFFE, http://www.ioffe.ru/SVA/NSM/Semicond/
        '''

        Ex = self.Ex0 - self.ExA * T**2 / (T + self.T0)

        return Ex

    def Nv (self, T):
        '''
        Effective density of states in the valence band

        Parameters from IOFFE,
        http://www.ioffe.ru/SVA/NSM/Semicond/
        '''

        Nv = self.Tv * T**1.5

        return Nv

    def Nc (self, T):
        '''
        Effective density of states in the conduction band

        Nc= 8.63·1013·T3/2 [1-1.9310-4·T-4.19·10-8·T2 +21·exp(-EΓL/(2kbT)) +44·exp(-EΓX/(2kbT)) (cm-3)

        Parameters from IOFFE,
        http://www.ioffe.ru/SVA/NSM/Semicond/GaAs/bandstr.html#Masses
        '''

        c1 = pc.q * (self.Eg(T) - self.El(T)) / 2 / pc.k / T
        c1 = self.Tc4 * np.exp(c1.magnitude)
        c2 = pc.q * (self.Eg(T) - self.Ex(T)) / 2 / pc.k / T
        c2 = self.Tc5 * np.exp(c2.magnitude)
        Nc = self.Tc0 * T**(1.5) * (self.Tc1 - self.Tc2 * T - self.Tc3 * T**2 + c1 + c2)

        return Nc

    def ni(self, T):
        '''
        intrinsic carrier density
        '''

        ni = Q_(2.1e6, 'cm**-3') # GaAs
        ni = np.sqrt (self.Nc (T) * self.Nv (T)) * np.exp (-self.Eg (T) / (2 * pc.k * T))

        return ni

    def carrDensity (self, u, T):

        return self.ni (T) * np.exp (pc.q * u / 2 / pc.k / T)

    def hwave(self, T):

        return self.Eg (T) + self.Chw * pc.k * T / pc.q

    def mobility (self, doping, T):

        RT = 300 * ureg.K
        a = self.muMax * (RT / T) ** self.theta1 - self.muMin
        b = 1 + (doping / (self.Nref * (T/RT)**self.theta2))**self.lamb
        mobility = self.muMin +  a/b

        return mobility

#########
class Layer:

    def __init__(
            self, name, material, dopingType, doping, thickness,
            isSurf=0, isAR=0, isSpreading=0
        ):
        self.name = name
        self.material = material
        self.dopingType = dopingType
        self.mobility = self.material.mobility
        self.doping = doping
        self.thickness = thickness
        self.isSurf = isSurf # does the layer contribute to surface resistance?
        self.isAR = isAR # is the layer the AR?
        self.isSpreading = isSpreading # is it a spreading layer?

    def resistance (self, rInt, rExt):
        return pf.rCirc (self.mobility(self.doping, T=300*ureg.K), self.doping, self.thickness, rInt, rExt)

    def carrDensity (self, u, T):
        return self.material.carrDensity (u, T)


class Stack:

    def __init__ (self, layers):
        self.layers = layers
        self.AR = None
        for layer in self.layers:
            if layer.isAR:
                self.AR = layer

    def rSurf (self, rInt, rExt):
        '''
        surface resistance
        '''

        layers_rsurf = np.zeros (len (self.layers)) * ureg.ohm

        for i, layer in enumerate(self.layers):
            layers_rsurf[i] = layer.resistance (rInt, rExt) if layer.isSurf \
                    else 0*ureg.ohm

        rSurf = 1. / layers_rsurf [layers_rsurf !=0]
        rSurf = 1 / np.sum (rSurf)

        return rSurf, layers_rsurf

    def rSpread (self, rInt, rExt):
        '''
        spreading resistance
        '''
        layers_rspread = np.zeros (len (self.layers)) * ureg.ohm
        for i, layer in enumerate(self.layers):
            layers_rspread[i] = layer.resistance (rInt, rExt) if layer.isSpreading \
                    else 0*ureg.ohm

        rSpread = 1. / layers_rspread [layers_rspread != 0]
        rSpread = 1 / np.sum (rSpread)

        return rSpread, layers_rspread


class Device:
    def __init__(self, **kwargs):

        #All devices parameters
        self.radius = kwargs['radius']
        self.ARthickness = kwargs['ARthickness']
        self.T = kwargs['T']
        self.rSh = kwargs['rSh']
        self.stack = kwargs['stack']
        self.A = kwargs['A']
        self.B = kwargs['B']
        self.C = kwargs['C']
        #  self.uSFDiode = kwargs['uSFDiode']
        self.uAR = kwargs['uAR']

        self.diameter = 2 * self.radius
        self.area = np.pi * self.radius**2

        #  for layer in self.stack.layers:
        #      layer.radius = self.radius


class LED (Device):

    def __init__ (self, **kwargs):

        super().__init__ (**kwargs)
        self.chi = kwargs['chi']
        self.dT = kwargs['dT']
        self.dM = kwargs['dM']
        self.Asurf = kwargs['Asurf']
        self.Bsurf = kwargs['Bsurf']
        self.Csurf = kwargs['Csurf']
        self.i0SF = kwargs['i0SF'] # Surface diode saturation current
        self.ARNdio = kwargs['ARNdio'] # number of distributed AR diodes

        self.dC = self.dT + self.dM

        self.Vt = pc.k * self.T / pc.q

        ############################################
        '''
        # Just one rSpread
        ##################
        self.rSF = rSurf (self.stack, self.radius, self.radius + self.dT)
        #  self.rSpread = 3*ureg.ohm + rCSL (self.stack, self.radius, self.dT, self.dM)
        self.rSP = rSpread (self.stack, self.radius, self.radius + self.dT + self.dM)

        # SF (surface) diode
        self.iSF = pf.iABC (
                    self.uSFDiode, self.Asurf, self.Bsurf, self.Csurf,
                    self.ARthickness, self.area, self.T
                )[0]
        self.uSFR = self.iSF * self.rSF #surface resistance

        # AR diode
        self.uAR = self.uSFDiode + self.uSFR
        self.iAR, self.iARsrh, self.iARrad, self.iARaug, self.nAR = \
                pf.iABC (
                        self.uAR, self.A, self.B, self.C,
                        self.ARthickness, self.area, self.T
                    )

        # Spread layer current and voltage
        self.iSP = self.iSF + self.iAR
        self.uSP = self.iSP * self.rSP

        # Total voltage through the LED
        self.u = self.uAR + self.uSP

        # Current through the shunt resistance
        self.iSh = self.u / self.rSh

        # Total current through the LED
        self.i = self.iSF + self.iAR + self.iSh
        #  '''

        ############################################

        '''
        # Splitted rSpread
        ##################
        self.rSF = rSurf (self.stack, self.radius - self.dT, self.radius)
        self.rSPT = rSpread (self.stack, self.radius - self.dT, self.radius)
        self.rSPM = rSpread (self.stack, self.radius, self.radius + self.dM)

        # AR diode
        self.nAR = pf.carrDensity ('GaAs', self.uAR, self.T)
        self.iAR, self.iARsrh, self.iARrad, self.iARaug = \
            pf.iABC (self.nAR, self.A, self.B, self.C, self.ARthickness, self.area)

        # Intermediate voltage
        self.u1 = self.uAR + self.iAR * self.rSPT

        # SF (surface) diode
        self.iSF = (2 * self.Vt.to_base_units().magnitude / self.rSF.to_base_units().magnitude) *\
                lambertw (
                        (self.i0SF.to_base_units().magnitude * self.rSF.to_base_units().magnitude / 2 / self.Vt.to_base_units().magnitude) * np.exp (self.u1.to_base_units().magnitude / 2 / self.Vt.to_base_units().magnitude))
        self.iSF = self.iSF.real * ureg.ampere
        self.uSF = self.u1 - self.iSF * self.rSF

        self.i = self.iAR + self.iSF

        # Spread layer voltage
        self.uSP = self.i * self.rSPM

        # Total voltage through the LED
        self.u = self.u1 + self.uSP

        #  # Current through the shunt resistance
        #  self.iSh = self.u / self.rSh
        #  # Total current through the LED
        #  self.i = self.iSF + self.iAR + self.iSh
        #  '''

        #  '''
        # Distributed diodes
        ####################


        self.rIntTop = self.radius - self.dT
        self.rExtTop = self.radius
        self.rIntMid = self.radius
        self.rExtMid = self.radius + self.dM

        # Surface resistance
        #  self.rSF = rSurf (self.stack, self.radius - self.dT, self.radius)
        self.rSF = self.stack.rSurf (self.rIntTop, self.rExtTop)[0]
        # Spread resistance
        #  self.rSP = rSpread (self.stack, self.radius, self.radius + self.dM)
        self.rSP = self.stack.rSpread (self.rIntMid, self.rExtMid)[0]


        # Distance between AR distributed diodes
        self.d0 = self.radius / self.ARNdio
        # Resistance between AR distributed diodes
        self.rARDio = np.zeros(self.ARNdio) * ureg.ohm
        # External radius of AR diodes
        self.radiusIntARDio = np.zeros (self.ARNdio) * ureg.micron
        self.radiusExtARDio = np.zeros (self.ARNdio) * ureg.micron
        # Area of AR distributed diodes
        self.areaDio = np.zeros (self.ARNdio) * ureg.micron**2
        # Carrier density in AR dist. diodes
        self.nARDio = np.zeros ((self.ARNdio, len(self.uAR))) * ureg['cm**-3']
        # Current throuugh AR distributed diodes
        self.iARDio =  np.zeros((self.ARNdio, len(self.uAR))) * ureg.ampere
        self.iARsrhDio =  np.zeros((self.ARNdio, len(self.uAR))) * ureg.ampere
        self.iARradDio =  np.zeros((self.ARNdio, len(self.uAR))) * ureg.ampere
        self.iARaugDio = np.zeros((self.ARNdio, len(self.uAR))) * ureg.ampere
        # Voltage accross AR dist. diodes
        self.uARDio = np.zeros ((self.ARNdio, len(self.uAR))) * ureg.volt
        # First (central) distributed diode voltage is the input parameter
        self.uARDio[0] = self.uAR

        for i in range(self.ARNdio):

            self.radiusIntARDio[i] = i * self.d0
            self.radiusExtARDio[i] = (i+1) * self.d0
            self.areaDio[i] =\
                    np.pi * (self.radiusExtARDio[i]**2 - self.radiusIntARDio[i]**2)

            #  print( self.stack.rSpread (self.radiusIntARDio[i], self.radiusExtARDio[i]))
            self.rARDio[i] = self.stack.rSpread (
                    self.radiusIntARDio[i], self.radiusExtARDio[i])[0]
            #  self.rARDio[i] = rSpread (self.stack, self.radiusIntARDio[i], self.radiusExtARDio[i])

            #  self.nARDio[i] = pf.carrDensity ('GaAs', self.uARDio[i], self.T)
            self.nARDio[i] = self.stack.AR.carrDensity (self.uARDio[i], self.T)
            self.iARDio[i], self.iARsrhDio[i], self.iARradDio[i], self.iARaugDio[i] = \
                pf.iABC (self.nARDio[i], self.A, self.B, self.C, self.ARthickness, self.areaDio[i])

            if i+1 < self.ARNdio:
                self.uARDio[i+1] = self.uARDio[i] + self.iARDio[i] * self.rARDio[i]

        # Voltage of the whole AR, after the last distributed diode and resistor
        self.uARn = self.uARDio[-1] + self.rARDio[-1]*self.iARDio[-1]

        # SF (surface) diode
        self.iSF = (2 * self.Vt.to_base_units().magnitude / self.rSF.to_base_units().magnitude) *\
                lambertw (
                        (self.i0SF.to_base_units().magnitude * self.rSF.to_base_units().magnitude / 2 / self.Vt.to_base_units().magnitude) * np.exp (self.uARn.to_base_units().magnitude / 2 / self.Vt.to_base_units().magnitude))

        self.iSF = self.iSF.real * ureg.ampere
        self.uSF = self.uARn - self.iSF * self.rSF

        # AR currents and carrier density
        self.nAR = self.nARDio.sum(axis=0)
        self.iAR = self.iARDio.sum(axis=0)
        self.iARsrh = self.iARsrhDio.sum(axis=0)
        self.iARrad = self.iARradDio.sum(axis=0)
        self.iARaug = self.iARaugDio.sum(axis=0)

        # Total current through the LED
        self.i = self.iAR + self.iSF

        # Spread layer voltage
        self.uSP = self.i * self.rSP

        # Total voltage through the LED
        self.u = self.uARn + self.uSP

        #  # Current through the shunt resistance
        #  self.iSh = self.u / self.rSh
        #  # Total current through the LED
        #  self.i = self.iSF + self.iAR + self.iSh
        #  '''

        #  '''

        ############################################

        # Efficiencies
        self.injEff = self.iAR / self.i # Injection efficiency
        self.extrEff = self.chi # Extraction efficiency
        #  self.iqe = self.iARrad  / (self.i- self.iARsrh) # Internal quantum efficiency
        self.iqe = self.iARrad  / self.i # Internal quantum efficiency
        self.eqe = self.extrEff * self.injEff * self.iqe # External quantum efficiency
        self.cqe = self.eqe
        self.cqeM = np.nanmax (self.cqe)
        # And CQE at the 'bandgap' voltage
        self.cqeGap = self.cqe[(self.u > Q_(1.42,'V')) & (self.u < Q_(1.48, 'V'))]
        self.cqeG = self.cqeGap[0] if len(self.cqeGap) > 0 else 0

        # Interpolate u and cqe so there is a give u value at, e.g., u=1.2
        minU=np.nanmin(self.uAR).magnitude
        maxU=np.nanmax(self.uAR).magnitude
        self.uVals = np.arange (minU, maxU, 0.01) * self.uAR.units
        #  print(self.uVals.units)
        self.uIntrp = np.interp (self.uVals, self.u, self.u) #* ureg.volt
        self.cqeIntrp = np.interp (self.uVals, self.u, self.cqe)
        self.cqegIntrp = self.cqeIntrp [np.round (self.uIntrp, 2) == Q_(1.42,'V')]

    def summary (self, saveTable=False, printTable=True):

        t = Texttable (max_width=0)


        data = [[
            'Layer Name',
            'Material',
            'Thickness (nm)',
            'Doping (cm-3))',
            'Mobility (cm2 / V s)',
            'Diam.',
            'dT',
            'dM',
            'R_layer (ohm)',
            'rSF (ohm)',
            'rSP (ohm)'
            ]]

        for layer in self.stack.layers:
            #  if not layer.isAR:
                data.append ([
                        layer.name,
                        layer.material.name,
                        layer.thickness.magnitude,
                        layer.doping.magnitude,
                        layer.mobility(layer.doping, T=300*ureg.K).magnitude,
                        self.diameter.magnitude,
                        self.dT.magnitude,
                        self.dM.magnitude,
                        layer.resistance(self.rIntTop, self.rExtTop).magnitude,
                        self.rSF.magnitude,
                        self.rSP.magnitude,
                    ])
            #  else:
            #      data.append ([
            #              layer.name,
            #              layer.material.name,
            #              self.diameter.magnitude,
            #              self.dT.magnitude,
            #              self.dM.magnitude,
            #              layer.thickness.magnitude,
            #              '-',
            #              layer.mobility(layer.doping, T=300*ureg.K).magnitude,
            #              '-',
            #              self.rSF.magnitude,
            #              self.rSP.magnitude,
            #          ])

        t.add_rows(data)

        t.set_cols_align (11*['c'])
        t.set_cols_valign(11*['t'])
        # t.set_chars ([horizontal, vertical, corner, header])
        # default is set to: ['-', '|', '+', '=']
        t.set_chars (['','|','','-'])
        t.set_deco (Texttable.VLINES | Texttable.BORDER)


        if printTable:
            print (t.draw())

        if saveTable:
            pass

        return t

# TODO: define Device subclasses for PD, and DDS
#  class PD(Device)
#  class DDS(Device):

class Pizza(Device):

    def __init__ (self, **kwargs):

        super().__init__ (**kwargs)

        self.FF = kwargs['FF']
        self.holeRad = kwargs['holeRad']
        #  self.holeSep = kwargs['holeSep']

        # A pizza device has multiple component devices (holes) in itself:
        kwargs.update({'radius': self.holeRad})
        self.hole = LED (**kwargs)

        # the comopnents occupy a total area of:
        self.areaHoles = self.area * self.FF
        # and their number is:
        self.nHoles = self.areaHoles / self.hole.area

        # the pizza parameters are calculated according to all the components/holes:
        self.u = self.hole.u
        self.i = self.nHoles * self.hole.i
        self.iRad = self.nHoles * self.hole.iLEDrad
        self.cqe = self.hole.iLEDrad / self.i

        #  print(type(self.cqe))
        self.cqeM = np.nanmax (self.cqe)
        #  print(f'CQEm = {self.hole.cqeM: .5f}')
        #  print(f'CQEm = {self.cqeM: .5f}')

        # TODO: not sure about this...
        self.rSF = (self.nHoles * self.hole.rSF ** -1) ** -1
        self.rSpread = (self.nHoles * self.hole.rSpread ** -1) ** -1

#  print(pizza.__mro__)


############################
# Device creation functions
############################
def createDevList (rads, dTs, dMs, **kwargs):
    devlist = []
    for rad in rads:
        for dT in dTs:
            for dM in dMs:
                addedkwargs = {'radius': rad, 'dT': dT, 'dM': dM}
                #  print(addedkwargs)
                kwargs = dict(kwargs,**addedkwargs)
                devlist.append (Device (**kwargs))
    return devlist

def createDevList2(devType, array_dict, startime=time(), trackTime=False, **kwargs):
    sample_list = []
    keys = []
    values = []
    for k, v in array_dict.items():
        keys.append(k)
        try:
            iter(v)
            values.append(v)
        except TypeError:
            values.append([v])

    for value_set in itertools.product(*values):
        kw = dict(zip(keys, value_set))
        kw.update(kwargs)
        #  print(kw)
        sample_list.append(devType(**kw))
        if trackTime:
            print(f'Elapsed {time() - startime:.3f} s: Device created')

    print(f'Elapsed {time()-startime:.3f} s. Number of devices: {len(sample_list)}')
    return sample_list


#####################
# Equivalent circuit
#####################
def ledECshockley (uSurfD, rSurf, i0Surf, i0LED, rSpread, rSh, i0PD, T):
    '''
    Calculate the current of an LED based on the simplest
    equivalent circuit considering the most basic internal caracteristics
    including a shunt resistance
    '''
    iSurf = pf.iShockley (uSurfD, i0Surf, 1, T) # surface
    uSurfR = iSurf * rSurf
    uLED = uSurfD + uSurfR
    iLED = pf.iShockley (uLED, i0LED, 1, T) # LED
    iSpread = iSurf + iLED
    uSpread = iSpread * rSpread
    #  iSpread = uSpread / rSpread
    u1 = uLED + uSpread
    iSh = u1 / rSh
    i = iSurf + iLED + iSh
    #  iPD = - pf.iShockley (u, i0PD, 2, T) # PD
    #  iP = 1
    #  i = iSurf + iLED + iSpread + iPD
    return {'uL': uLED, 'u': u1, 'i':i, 'iSurf':iSurf, 'iL':iLED, 'iSh': iSh, 'iSpread':iSpread}

def ledEC (uSurfD, A, B, C, Asurf, Bsurf, Csurf, rSpread, rSurf, rSh, ARthickness, area, T):
    '''
    Calculate the current of an LED based on the simplest
    equivalent circuit considering the most basic internal caracteristics
    including a shunt resistance,
    using the ABC model for the diode currents
    '''

    # Surface diode
    iSurf = pf.iABC (uSurfD, Asurf, Bsurf, Csurf, ARthickness, area, T)[0]
    uSurfR = iSurf * rSurf

    # LED diode
    uLED = uSurfD + uSurfR
    iLED, iLEDsrh, iLEDrad, iLEDaug, nLED = pf.iABC (uLED, A, B, C, ARthickness, area, T)

    iSpread = iSurf + iLED
    uSpread = iSpread * rSpread

    u1 = uLED + uSpread
    iSh = u1 / rSh
    i = iSurf + iLED + iSh
    cqe = iLEDrad / i
    #  iPD = - pf.iShockley (u, i0PD, 2, T) # PD
    #  iP = 1
    #  i = iSurf + iLED + iSpread + iPD
    #  return uLED, u1, uSpread, uSurfR, i, iSurf, iLED, iLEDsrh, iLEDrad, iLEDaug, iSh, iSpread, cqe#, iPD
    return {'uL': uLED, 'u': u1, 'uSpread': uSpread, 'uSurfR': uSurfR, 'i':i, 'iSurf':iSurf, 'iL':iLED, 'iLsrh': iLEDsrh, 'iLrad':iLEDrad, 'iLaug':iLEDaug, 'iSh': iSh, 'iSpread':iSpread, 'cqe':cqe}#, iPD
